<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Payout_model extends CI_Model
{
    public function get_all_payouts($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by('id',"DESC");
        $query = $this->db->get('payout_tracking');
        return $query->result();
    }

    public function add_payout($data)
    {
        return $this->db->insert('payout_tracking', $data);
    }

    //delete contact message
    public function delete_payout($id)
    {
        $id = clean_number($id);
        $this->db->where('id', $id);
        return $this->db->delete('payout_tracking');
    }

    //get project by id
    public function get_payout_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('payout_tracking');
        return $query->row();
    }

    public function payout_update($data,$id)
    {
        $this->db->where('id', $id);
        return $this->db->update('payout_tracking', $data);
    }
    public function get_all_payouts_count($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by('id', "DESC");
        $query = $this->db->get('payout_tracking');
        return $query->num_rows();
    }
    /*
    *-------------------------------------------------------------------------------------------------
    * Reward Transactions
    *-------------------------------------------------------------------------------------------------
    */
    public function get_all_rewards($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by('id', "DESC");
        $query = $this->db->get('company_rewad_tracking');
        return $query->result();
    }

    public function add_reward($data)
    {
        return $this->db->insert('company_rewad_tracking', $data);
    }

    //delete contact message
    public function delete_reward($id)
    {
        $id = clean_number($id);
        $this->db->where('id', $id);
        return $this->db->delete('company_rewad_tracking');
    }

    //get project by id
    public function get_reward_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('company_rewad_tracking');
        return $query->row();
    }

    public function reward_update($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('company_rewad_tracking', $data);
    }
    public function get_all_rewards_count($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by('id', "DESC");
        $query = $this->db->get('company_rewad_tracking');
        return $query->num_rows();
    }
    /*
    *-------------------------------------------------------------------------------------------------
    * Cashback Transactions
    *-------------------------------------------------------------------------------------------------
    */
    public function get_all_cashbacks($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by('id', "DESC");
        $query = $this->db->get('cashback_user_trans');
        return $query->result();
    }

    public function add_cashback($data)
    {
        return $this->db->insert('cashback_user_trans', $data);
    }

    //delete contact message
    public function delete_cashback($id)
    {
        $id = clean_number($id);
        $this->db->where('id', $id);
        return $this->db->delete('cashback_user_trans');
    }

    //get project by id
    public function get_cashback_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('cashback_user_trans');
        return $query->row();
    }

    public function cashback_update($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('cashback_user_trans', $data);
    }
    public function get_all_cashbacks_count($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by('id', "DESC");
        $query = $this->db->get('cashback_user_trans');
        return $query->num_rows();
    }
    
}
