<div class="wrapper">
    <div class="col-sm-12">
        <?php $this->load->view('admin/includes/_messages'); ?>
    </div>
    <div class="">
        <div class="wrapper-box">
            <h1 class="text-center">Application Details</h1>
            <div class="wrapper-box-content">
                <div class="m-wrapper-content">
                    <div class="captcha">
                        <div class="captcha-content">
                            <div class="table-responsive ">
                                <table class="table table-striped table-bordered">
                                    <tr>
                                        <td>User ID</td>
                                        <td><strong class='text-primary'><?php echo $user_data->username ?></strong></td>
                                        <td>Sponser ID</td>
                                        <td><strong class='text-primary'><?php echo getUsernameById($user_data->spon_id) ?></strong></td>
                                    <tr>
                                        <td>Membership Status</td>
                                        <td><?php if ($user_data->is_approved == "0") {
                                                echo "<strong class='text-info'>Pending </strong>";
                                            } else  if ($user_data->is_approved == "1") {
                                                echo "<strong class='text-success'>Approved</strong>";
                                            } else  if ($user_data->is_approved == "2") {
                                                echo "<strong class='text-danger'>Rejected</strong>";
                                            }
                                            ?>
                                        </td>
                                        <td>Application Fees Status</td>
                                        <td><?php if ($user_data->is_paid == "1") {
                                                echo "<strong class='text-success'>Paid</strong>";
                                            } else  if ($user_data->is_approved == "0") {
                                                echo "<strong class='text-danger'>Pending</strong>";
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php if ($user_data->is_approved == "2") : ?>
                                        <td>Rejection Cause</td>
                                        <td colspan="3">
                                            <?php echo $user_data->rejection_cause ?>
                                        </td>
                                    <?php endif; ?>
                                    <tr>
                                        <td>Name Of Applicant</td>
                                        <td><?php echo $user_data->full_name ?></td>
                                        <td>Father's/husband's Name</td>
                                        <td><?php echo $user_data->father_name ?></td>
                                    </tr>
                                    <tr>
                                        <td>Date Of Birth</td>
                                        <td><?php echo date('d-M-Y', strtotime($user_data->dob))  ?></td>
                                        <td>AGE</td>
                                        <td><?php echo $user_data->age  ?></td>

                                    </tr>
                                    <tr>
                                        <td>Occupation</td>
                                        <td><?php echo ucfirst($user_data->occupation)  ?></td>

                                        <td>PAN</td>
                                        <td><?php echo strtoupper($user_data->pan) ?></td>
                                    </tr>
                                    <tr>
                                        <td>Location</td>
                                        <td>
                                            State :<?php echo $user_data->state; ?> <br>
                                            Pin : <?php echo $user_data->pin ?>
                                        </td>
                                        <td>Contact Details</td>
                                        <td>
                                            Mobile: <?php echo $user_data->mobile; ?><br>
                                            Phone: <?php echo $user_data->phone; ?><br>
                                            email: <?php echo $user_data->email; ?><br>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>PAN</td>
                                        <td><?php echo $user_data->pan ?></td>
                                        <td> Application Date</td>
                                        <td colspan="3"> <?php echo date('d-M-Y', strtotime($user_data->created_on)) ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <table class="table">
                                                <tr>
                                                    <th>Photo</th>
                                                    <th>Signature</th>
                                                    <th>PAN Card</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <?php if (!empty($user_data->avatar)) : ?>
                                                            <img src="<?php echo base_url() . 'uploads/member-doc/' . $user_data->avatar ?>" style="width: 100%;padding: 10px 0;">

                                                            <a target="_blank" href="<?php echo base_url() . 'uploads/member-doc/' . $user_data->avatar ?>" title="Download" class="btn btn-success btn-sm mx-1" download><i class="fa fa-download" aria-hidden="true"></i></a>

                                                            <a target="_blank" href="<?php echo base_url() . 'uploads/member-doc/' . $user_data->avatar ?>" title="View" class="btn btn-warning btn-sm mx-1"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        <?php else : ?>
                                                            <img src="<?php echo base_url() . 'assets/backend/images/noimg.png' ?>" style="width: 100%;padding: 10px 0;">
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php if (!empty($user_data->sign_img)) : ?>
                                                            <img src="<?php echo base_url() . 'uploads/member-doc/' . $user_data->sign_img ?>" style="width: 100%;padding: 10px 0;">

                                                            <a target="_blank" href="<?php echo base_url() . 'uploads/member-doc/' . $user_data->sign_img ?>" title="Download" class="btn btn-success btn-sm mx-1" download><i class="fa fa-download" aria-hidden="true"></i></a>

                                                            <a target="_blank" href="<?php echo base_url() . 'uploads/member-doc/' . $user_data->sign_img ?>" title="View" class="btn btn-warning btn-sm mx-1"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        <?php else : ?>
                                                            <img src="<?php echo base_url() . 'assets/backend/images/noimg.png' ?>" style="width: 100%;padding: 10px 0;">
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php if (!empty($user_data->pan_img)) : ?>
                                                            <img src="<?php echo base_url() . 'uploads/member-doc/' . $user_data->pan_img ?>" style="width: 100%;padding: 10px 0;">

                                                            <a target="_blank" href="<?php echo base_url() . 'uploads/member-doc/' . $user_data->pan_img ?>" title="Download" class="btn btn-success btn-sm mx-1" download><i class="fa fa-download" aria-hidden="true"></i></a>

                                                            <a target="_blank" href="<?php echo base_url() . 'uploads/member-doc/' . $user_data->pan_img ?>" title="View" class="btn btn-warning btn-sm mx-1"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        <?php else : ?>
                                                            <img src="<?php echo base_url() . 'assets/backend/images/noimg.png' ?>" style="width: 100%;padding: 10px 0;">
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="sav-btn">
                                <?php if ($user_data->is_paid == 0) : ?>
                                    <a class="btn btn-primary" href="<?php echo admin_url() . 'make-payment/' . $user_data->id ?>">Receive Payment</a>
                                <?php endif; ?>
                                <?php if ($user_data->is_approved == 0) : ?>
                                    <a class="btn btn-success" href="<?php echo admin_url() . 'approve-member/' . $user_data->id ?>">Approve</a>

                                    <a class="btn btn-warning" href="<?php echo admin_url() . 'reject-member/' . $user_data->id ?>">Reject</a>

                                    <a class="btn btn-danger" href="javascript:void(0)" onclick="delete_item('application_controller/delete_application','<?php echo $user_data->id; ?>','Are you want to delete this item?');">Delete</a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>