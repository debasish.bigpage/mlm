   <div class="col-sm-12">
       <?php $this->load->view('admin/includes/_messages'); ?>
   </div>

   <div class="wrapper2">

       <div class="mblog">

           <h4><?php echo $title ?></h4>
           <?php echo form_open('admin/update-reward-plan'); ?>
           <div class="form-group">
               <label>Step Leavel</label>
               <select name="level_step" class="form-control" required>
                   <option value="">select One </option>
                   <?php foreach ($business_settings as $steps) : ?>
                       <option value="<?php echo $steps->id; ?>" <?php echo ($steps->level_step == $member->level_step) ? "selected" : ""; ?>><?php echo $steps->designation . ' - ' . $steps->id; ?></option>
                   <?php endforeach; ?>
               </select>
           </div>

           <div class="form-group">
               <label>Time (In Days)</label>
               <input type="number" name="timeline" class="form-control" required value="<?php echo $member->timeline ?>">
           </div>
           <div class="form-group">
               <label>Reward Name</label>
               <input type="text" name="reward_name" class="form-control" required value="<?php echo $member->reward_name ?>">
           </div>
           <div class="form-group">
               <label>Price Amount</label>
               <input type="text" name="price" class="form-control" required value="<?php echo $member->price ?>">
           </div>
       </div>
       <input type="hidden" name="id" value="<?php echo $member->id ?>">
        <div class="sav-btn">
       <button type="submit">Save Reward Plan</button>
   </div>
   <?php echo form_close(); ?>
   </div>