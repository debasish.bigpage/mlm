<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Cron_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
      $this->calculateTotalLeg();
      $this->compleateLegSet();
    }

    public function calculateTotalLeg()
    {
        $where = array(
            'is_approved' => 1, 'banned'=>0
        );
        $userList = $this->authication_model->getUserListResult($where);
        if(!empty($userList)){
            foreach ($userList as $users) {
                $parrent_id = $users->id;
                $legCount = $this->authication_model->get_cond_users_count(array('is_approved' => 1, 'parrent_id' => $parrent_id));
                //update Leg count
                if($legCount!=0){
                    $userArray = array(
                        'total_leg' => $legCount,
                    );
                        $this->authication_model->updateUsers($userArray, $parrent_id);    
                }
            }
            
        }
    }
    public function compleateLegSet()
    {
        $userList = $this->network_model->getNetworkUserList();
        foreach ($userList as $user) {
            if($user->l1>0 && $user->l2 > 0 && $user->r1 > 0 && $user->r2 > 0){
                $newtworkUpdateArr = array('is_my_set_compleate'=>1);
                $this->network_model->update_network($newtworkUpdateArr, $user->id);
            }
        }
    }


    public function setCashback()
    {
        $cashbackData = $this->cashback_model->get_cashback_settings();
        $where = array(
            'is_approved' => 1, 'banned' => 0 , 'total_leg >=' => $cashbackData->total_member
        );
        $userList = $this->authication_model->getUserListResult($where);
        if (!empty($userList)) {
            foreach ($userList as $users) {
                $cashbackUserExists = $this->cashback_model->get_cashback_user_by_id($users->id);
                if(empty($cashbackUserExists)){
                    $insertArr = [
                        'user_id'=> $users->id,
                    ];
                    $this->cashback_model->insert_cashback_urser($insertArr);
                }
            }
        }
        $this->do_cashbackTrans(); 
        
    }
    protected function do_cashbackTrans(){
         $users = $this->cashback_model->get_all_cashback_user();
        $cashbackData = $this->cashback_model->get_cashback_settings();
        if(!empty($users)){
            foreach ($users as $user) {
                $cashbackInsertArr = [
                    'user_id' => $user->user_id,
                    'amount' => $cashbackData->amount,
                    'description' => "Monthly cashback item send to ". getUsernameById($user->user_id)." for the month of ".date('M-Y')." amounted ".$cashbackData->amount,
                    'created_at' => date("Y-m-d H:i:s"),
                    'status' => 0,
                ];
                $this->payout_model->add_cashback($cashbackInsertArr);
            }
        }
    }

    
}

//UPDATE `users` SET `is_set_leg`=0 WHERE 1
