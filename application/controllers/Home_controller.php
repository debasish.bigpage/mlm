<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home_controller extends CI_Controller {
	function __construct () {
		parent::__construct();
	}
	public function index()
	{
		$data['title'] = $this->settings->homepage_title;
		$data['description'] = $this->settings->site_description;
		$data['keywords'] = $this->settings->keywords;
		$data['feachered_businesses'] = $this->business_model->getFeacheredBusiness();
		$data['about_page'] = $this->page_model->get_page_by_id(1);
		$this->load->view('frontend/includes/header',$data);
		$this->load->view('index');	
		$this->load->view('frontend/includes/footer');
	}
}