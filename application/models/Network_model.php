<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Network_model extends CI_Model
{
    public function add_to_network($data)
    {
        return $this->db->insert('network', $data);
    }

    public function checkLegisFreeByUserID($user_id)
    {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('network');
        return $query->row();
    }

    public function getNetworkUserList($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get('network');
        return $query->result();
    }

    public function update_network($data,$id)
    {
       $this->db->where('id', $id);
       return $this->db->update('network', $data);
       
    }

}
?>