<div class="wrapper2">
    <div class="col-sm-12">
        <?php $this->load->view('admin/includes/_messages'); ?>
    </div>
    <div class="mblog-post">
        <a href="<?php echo admin_url() ?>add-reward-plan" class="add-pag">Add Reward Plan</a>
        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable_lang" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Step Level</th>
                        <th>Time</th>
                        <th>Reward Name </th>
                        <th>Price </th>
                        <th>Option</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($business_settings as $item) :
                    ?>
                        <tr>
                            <td><?php echo html_escape($item->id); ?></td>
                            <td>Step -<?php echo html_escape($item->level_step); ?></td>
                            <td><?php echo html_escape($item->timeline); ?></td>
                            <td><?php echo html_escape($item->reward_name); ?></td>
                            <td><?php echo html_escape($item->price); ?></td>
                            <td class="drp-btn">
                                <div class="dropdown drp">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select a Option
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="<?php echo admin_url(); ?>edit-reward-plan/<?php echo html_escape($item->id); ?>">Edit <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <a class="dropdown-item" href="javascript:void(0)" onclick="delete_item('admin/delete_reward_plan','<?php echo $item->id; ?>','Are you want to delete this item?');">Delete <i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Step Level</th>
                        <th>Time</th>
                        <th>Reward Name </th>
                        <th>Price </th>
                        <th>Option</th>
                    </tr>
                </tfoot>
            </table>
            <div class="text-right madd-btn">
                <a href="<?php echo admin_url() ?>add-reward-plan"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Reward Plan</a>
            </div>
        </div>
    </div>
</div>