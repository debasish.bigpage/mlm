<div class="wrapper2">
    <?php $this->load->view('admin/includes/_messages'); ?>
    <div class="mblog-post">
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load('current', {
                packages: ["orgchart"]
            });
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Name');
                data.addColumn('string', 'Manager');
                data.addColumn('string', 'ToolTip');

                // For each orgchart box, provide the name, manager, and tooltip to show.
                data.addRows([
                    [{
                        'v': 'Mike',
                    }, '', 'The President'],
                    [{
                        'v': 'Jim',
                    }, 'Mike', 'VP'],
                    [{
                        'v': 'tim',
                    }, 'Mike', 'VP'],
                    [{
                        'v': 'dim',
                    }, 'Mike', 'VP'],
                    ['Alice', 'Mike', ''],
                    ['Bob', 'Mike', 'Bob Sponge'],
                    ['Carol', 'Bob', '']
                ]);

                // backup code
                /*   data.addRows([
                      [{
                              'v': 'Mike',
                              'f': 'Mike<div style="color:red; font-style:italic">President</div>'
                          },
                          '', 'The President'
                      ],
                      [{
                              'v': 'Jim',
                              'f': 'Jim<div style="color:red; font-style:italic">Vice President</div>'
                          },
                          'Mike', 'VP'
                      ],
                      ['Alice', 'Mike', ''],
                      ['Bob', 'Jim', 'Bob Sponge'],
                      ['Carol', 'Bob', '']
                  ]); */

                // Create the chart.
                var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
                // Draw the chart, setting the allowHtml option to true for the tooltips.
                chart.draw(data, {
                    'allowHtml': true
                });
            }
        </script>
        <div id="chart_div"></div>
    </div>
</div>