<div class="wrapper">
    <div class="">
        <?php echo form_open_multipart('admin/bank_settings_post'); ?>
        <div class="wrapper-box">
            <h1 class="text-center">Bank Settings</h1>
            <div class="col-sm-12">
                <?php $this->load->view('admin/includes/_messages'); ?>
            </div>
            <div class="wrapper-box-content">
               
                <div class="m-wrapper-content">

                    <div class="captcha">
                        <div class="captcha-content">
                            <div class="form-group">
                                <label>Beneficiary Name</label>
                                <input type="text" name="account_name" class="form-control" value="<?php echo $bank_settings->account_name; ?>" placeholder="Beneficiary Name">
                            </div>
                            <div class="form-group">
                                <label>Bank Name</label>
                                <input type="text" name="bank_name" class="form-control" value="<?php echo $bank_settings->bank_name; ?>" placeholder="Bank Name">
                            </div>
                            <div class="form-group">
                                <label>Beneficiary Account Number</label>
                                <input type="text" name="account_no" class="form-control" value="<?php echo $bank_settings->account_no; ?>" placeholder="Beneficiary  Account Number">
                            </div>
                            <div class="form-group">
                                <label>Beneficiary IFSC Code</label>
                                <input type="text" name="ifsc_code" class="form-control" value="<?php echo $bank_settings->ifsc_code; ?>" placeholder="Beneficiary IFSC Code">
                            </div>
                            <div class="form-group">
                                <label>Bank branch Address</label>
                                <input type="text" name="branch_address" class="form-control" value="<?php echo $bank_settings->branch_address; ?>" placeholder="Bank branch Address">
                            </div>
                         <div class="sav-btn">
                                <button>Save Changes</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>