<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Businessplan_model extends CI_Model
{
    protected function input_values()
    {
        $data = array(
            'level_step' => $this->input->post('level_step', true),
            'designation' => $this->input->post('designation', true),
            'left_leg' => $this->input->post('left_leg', true),
            'right_leg' => $this->input->post('right_leg', true),
            'total_leg' => $this->input->post('right_leg', true)+ $this->input->post('left_leg', true),
            'anount' => $this->input->post('anount', true),
        );
        return $data;
    }

    public function get_all_business_plans($where='')
    {
        if(!empty($where)){
            $this->bp->where($where);
        }
        $query = $this->db->get('business_settings');
        return $query->result();
    }

    public function add_business_plan()
    {
        $data =$this->input_values();
       return $this->db->insert('business_settings',$data);
    }

    //delete contact message
    public function delete_business_plan($id)
    {
        $id = clean_number($id);
        $this->db->where('id', $id);
        return $this->db->delete('business_settings');
    }

    //get project by id
    public function get_business_plan_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('business_settings');
        return $query->row();
    }

    public function update($id)
    {
        //set values
        $data = $this->input_values();
        $project = $this->get_business_plan_by_id($id);
        if (!empty($project)) {
            $this->db->where('id', $id);
            return $this->db->update('business_settings', $data);
        }
        return false;
    }


}