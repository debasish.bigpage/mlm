<div class="wrapper2">

    <?php $this->load->view('admin/includes/_messages'); ?>

    <div class="mblog-post">

        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>UserID</th>
                        <th>Name</th>
                        <th>Paid</th>
                        <th>Date</th>
                        <th>Option</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($applications as $item) : ?>
                        <tr>
                            <td><?php echo html_escape($item->id); ?></td>
                            <td><?php echo html_escape($item->username); ?></td>
                            <td><?php echo html_escape($item->full_name); ?></td>
                            <td>
                                <?php if ($item->is_paid == 1) : ?>
                                    <strong class="text-success">Paid</strong>
                                <?php else : ?>
                                    <strong class="text-danger">Un-Paid</strong>
                                <?php endif; ?>
                            </td>
                            <td><?php echo formatted_date($item->created_on); ?></td>
                            <td class="drp-btn">
                                <div class="dropdown drp">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select a Option
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="<?php echo admin_url() . 'view-application/' . $item->id ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View </a>
                                        <?php if ($item->is_paid == 0) : ?>
                                            <a class="dropdown-item" href="<?php echo admin_url() . 'make-payment/' . $item->id ?>"><i class="fa fa-inr" aria-hidden="true"></i> Recive Payment </a>
                                        <?php endif; ?>
                                        <a class="dropdown-item" href="<?php echo admin_url() . 'reject-application/' . $item->id ?>"><i class="fa fa-ban" aria-hidden="true"></i> Reject Application </a>
                                        <?php if ($item->is_paid == 1) : ?>
                                            <a class="dropdown-item" href="<?php echo admin_url() . 'approve-member/' . $item->id ?>"><i class="fa fa-check-square" aria-hidden="true"></i> Accept Application </a>
                                        <?php endif; ?>
                                        <?php if ($item->is_approved == 0) : ?>
                                            <a class="dropdown-item" href="javascript:void(0)" onclick="delete_item('application_controller/delete_application','<?php echo $item->id; ?>','Are you want to delete this application?');"><i class="fa fa-trash" aria-hidden="true"></i> Delete </a>
                                        <?php endif; ?>

                                    </div>
                                </div>
                            </td>
                        </tr>

                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>UserID</th>
                        <th>Name</th>
                        <th>Paid</th>
                        <th>Date</th>
                        <th>Option</th>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>