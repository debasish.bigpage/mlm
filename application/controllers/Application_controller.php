<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Application_controller extends Home_Core_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

   public function pending()
   {
        $data['title'] = "pending Member Application";
        $data['applications'] =  $this->authication_model->getCustomUserList(array('is_approved' => 0));
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/member/pending');
        $this->load->view('admin/includes/footer');
   }

    public function reject()
    {
        $data['title'] = "Rejected Member Application";
        $data['applications'] =  $this->authication_model->getCustomUserList(array('is_approved' => 2));
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/member/reject');
        $this->load->view('admin/includes/footer');
    }

    public function view_application($id)
    {
        if (!is_admin()) {
            redirect('dashboard');
        }
        $data['title'] = "View Application";
        $data['user_data'] =  $this->authication_model->get_user($id);
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/member/viewApplication');
        $this->load->view('admin/includes/footer');
    }
    /*
    *-------------------------------------------------------------------------------------------------
    * Application FEE
    *-------------------------------------------------------------------------------------------------
    */
    public function make_payment($id)
    {
        if (!is_admin()) {
            redirect('dashboard');
        }
        $data['title'] = "Application payment";
        $data['app_data'] =  $this->authication_model->get_user($id);
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/member/make_payment');
        $this->load->view('admin/includes/footer');
    }

    // receive payment
    public function make_payment_post()
    {
        $this->form_validation->set_rules('utrn', "Transaction / UTRN Number", 'required|xss_clean|is_unique[application_fees_trans.utrn]');
        $this->form_validation->set_rules('trans_date', "Date", 'required|xss_clean');
        $this->form_validation->set_rules('amount', "Amount", 'required|xss_clean');
        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            $utrn = $this->input->post('utrn',true);
            $amount = $this->input->post('amount',true);
            $trans_date = $this->input->post('trans_date',true);
            $id = $this->input->post('id',true);
            $appicationPayment = array(
                'utrn'=>$utrn,
                'amount'=>$amount,
                'trans_date'=>$trans_date,
                'user_id'=> $id,
            );
            $transactionRegisterArr = array(
                'form_user_id' => $id,
                'to_user_id' => 1,
                'tranaction_date' => $trans_date,
                'created_on' => date("Y-m-d H:i:s"),
                'dr' => $amount,
                'cr' => 0,
                'perticullars' => 'application fees received form '. getUsernameById($id).', trans-id : '.$utrn,
            );
            $userArray = array(
                'is_paid'=>1
            );
            if ($this->authication_model->updateUsers($userArray, $id)) {
                //add transaction
                $this->transaction_model->add_transaction($transactionRegisterArr);
                $this->transaction_model->add_app_fee($appicationPayment);
                $this->session->set_flashdata('success', "Payment Received");
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('error', "Unable To receive Payment ");
                redirect($this->agent->referrer());
            }
        }

    }
    /*
    *-------------------------------------------------------------------------------------------------
    * Application Rejection
    *-------------------------------------------------------------------------------------------------
    */
    public function reject_member($id)
    {
        if (!is_admin()) {
            redirect('dashboard');
        }
        $data['title'] = "Reject Application";
        $data['app_data'] =  $this->authication_model->get_user($id);
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/member/reject_app');
        $this->load->view('admin/includes/footer');
    }

    public function reject_member_post()
    {
        $this->form_validation->set_rules('rejection_cause', "Rejection Cause", 'required|xss_clean');
        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            $rejection_cause = $this->input->post('rejection_cause', true);
            $id = $this->input->post('id', true);
            
            $userArray = array(
                'is_approved' => 2,
                'rejection_cause'=> $rejection_cause
            );
            if ($this->authication_model->updateUsers($userArray, $id)) {
                $this->session->set_flashdata('success', "Application Rejected");
                redirect('admin/view-application/' . $id);
            } else {
                $this->session->set_flashdata('error', "Unable To reject");
                redirect($this->agent->referrer());
            }
        }
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * Application Rejection
    *-------------------------------------------------------------------------------------------------
    */
    public function delete_application()
    {
        if (!is_admin()) {
            redirect('dashboard');
        }
        $id = $this->input->post('id', true);
        $userData = $this->authication_model->get_user($id);
        if(!empty($userData)){
            delete_file_from_server('uploads/member-doc/' . $userData->avatar);
            delete_file_from_server('uploads/member-doc/' . $userData->pan_img);
            delete_file_from_server('uploads/member-doc/' . $userData->sign_img);
            if ($this->authication_model->delete_user($id)) {
                $this->session->set_flashdata('success', "Application Deleted");
            } else {
                $this->session->set_flashdata('error', "Unable To delete Application");
            }
        }else{
            $this->session->set_flashdata('error', "No Such Application Found");
        }
    }
    /*
    *-------------------------------------------------------------------------------------------------
    * Application Apporve
    *-------------------------------------------------------------------------------------------------
    */
    public function approve_member($id)
    {
        if (!is_admin()) {
            redirect('dashboard');
        }
        $userData = $this->authication_model->get_user($id);
        if(!empty($userData)){
            //user table
            $userArray = array(
                'is_approved' => 1,
                'approved_on' => date('Y-m-d H:i:s'),
                'parrent_id'=>$userData->spon_id,
            );
            // network table
            
            $networkInsertArr = array(
                'user_id'=>$userData->id,
                'parrent_id' => $userData->spon_id,
            );
           
            if ($this->authication_model->updateUsers($userArray, $id)) {
                $this->network_model->add_to_network($networkInsertArr);
                $this->SetNetWork($userData->spon_id);
                $this->session->set_flashdata('success', "Application Approved");
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('error', "Unable To Approved");
                redirect($this->agent->referrer());
            }
        }else{
            $this->session->set_flashdata('error', "No Such Application Found");
            redirect($this->agent->referrer());
        }
    }

    /*
   NETWORK SECTION START
   */
    public function SetNetWork($parrent_id)
    {
        $where = array(
            'is_approved' => 1, 'parrent_id' => $parrent_id, 'is_set_leg' => 0
        );
        $userLists = $this->authication_model->getUserListResult($where);
        $userListsIDS = $this->getChildIdAgenstParrentID($parrent_id);
        $existingIDS = $this->getNetWorkexistingIDS();
        foreach ($userLists as $userList) {
            $userTableID = $userList->id;
            $legFinder = $this->checkLegisFree($parrent_id);
            if ($legFinder['status'] == 1) {
                $newtworkUpdateArr = array(
                    $legFinder['pos'] => $userList->id,
                );
                $userArray = array(
                    'is_set_leg' => 1,
                );
                $this->network_model->update_network($newtworkUpdateArr, $legFinder['row_id']);
                $this->authication_model->updateUsers($userArray, $userTableID);
            } else {
                $legFinder = $this->subLegFinder($userListsIDS, $existingIDS);
                if ($legFinder['status'] == 1) {
                    $newtworkUpdateArr = array(
                        $legFinder['pos'] => $userList->id,
                    );
                    $userArray = array(
                        'is_set_leg' => 1,
                    );
                    $this->network_model->update_network($newtworkUpdateArr, $legFinder['row_id']);
                    $this->authication_model->updateUsers($userArray, $userTableID);
                }
            }
        }
    }

    public function subLegFinder($userListsIDS, $existingIDS)
    {
        foreach ($userListsIDS as $clildID) {
            $legFinder = $this->checkLegisFree($clildID);
            if ($legFinder['status'] == 1) {
                return $legFinder;
            }
        }
    }

    public function checkLegisFree($parrent_id)
    {
        $returnData = array();
        $parrentuserData = $this->network_model->checkLegisFreeByUserID($parrent_id);
        if (($parrentuserData->is_my_set_compleate == 0)) {
            //check the leg 
            if ($parrentuserData->l1 == 0) {
                $returnData['status'] = 1;
                $returnData['pos'] = 'l1';
                $returnData['user_id'] = $parrent_id;
                $returnData['row_id'] = $parrentuserData->id;
            } elseif ($parrentuserData->l2 == 0) {
                $returnData['status'] = 1;
                $returnData['pos'] = 'l2';
                $returnData['user_id'] = $parrent_id;
                $returnData['row_id'] = $parrentuserData->id;
            } elseif ($parrentuserData->r1 == 0) {
                $returnData['status'] = 1;
                $returnData['pos'] = 'r1';
                $returnData['user_id'] = $parrent_id;
                $returnData['row_id'] = $parrentuserData->id;
            } elseif ($parrentuserData->r2 == 0) {
                $returnData['status'] = 1;
                $returnData['pos'] = 'r2';
                $returnData['user_id'] = $parrent_id;
                $returnData['row_id'] = $parrentuserData->id;
            } else {
                $returnData['status'] = 0;
            }
        } else {
            $returnData['status'] = 2;
        }
        return $returnData;
    }

    public function getChildIdAgenstParrentID($parrent_id)
    {
        $ids = array();
        $where1 = array(
            'is_approved' => 1, 'parrent_id' => $parrent_id,
        );
        $userListsIDS = $this->authication_model->getUserListResult($where1);
        foreach ($userListsIDS as $childID) {
            array_push($ids, $childID->id);
        }
        return $ids;
    }

    public function getNetWorkexistingIDS()
    {
        $ids = array();
        $userListsIDS = $this->network_model->getNetworkUserList();
        foreach ($userListsIDS as $childID) {
            $l1 = $childID->l1;
            $l2 = $childID->l2;
            $l3 = $childID->r1;
            $l4 = $childID->r2;
            array_push($ids, $l1, $l2, $l3, $l4);
        }
        return array_unique($ids);
    }

    /*
    *NETWORK SECTION END
    */


   /*  public function createDummyAccount()
    {
        $this->load->library('bcrypt');
        for ($i = 91; $i < 101; $i++) {
            
            $username = 'ACM22C310'.$i;
            $data = array(
                'username' => $username,
                'full_name' => "Test User" . $i,
                'password' => $this->bcrypt->hash_password($i),
                'dob' => date('Y-m-d'),
                'sex' => 'male',
                'pan' => "PAN" . $i,
                'mobile' => $i,
                'age' => 15,
                'email' => "test_user" . $i . "@bigpage.in",
                'spon_id' => 20, // need to cahange every time  
                'last_seen' => date('Y-m-d H:i:s'),
                'created_on' => date('Y-m-d H:i:s'),
                'approved_on' => date('Y-m-d H:i:s'),
                'is_approved' => 1,
                'email_status' => 1,
                'role' => 'member',
                'token' => generate_token(),
                'user_type' => 'registered',
                'slug' => $username,

            );
          
            
           $this->authication_model->register($data);
           $Lid = $this->db->insert_id();
           $this->approve_member($Lid);
        }
    } */

}
