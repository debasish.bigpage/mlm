   <div class="col-sm-12">
       <?php $this->load->view('admin/includes/_messages'); ?>
   </div>

   <div class="wrapper2">

       <div class="mblog">

           <h4><?php echo $title ?></h4>
           <?php echo form_open('admin/save-business-plan'); ?>

           <div class="form-group">
               <label>Step Leavel</label>
               <input type="number" name="level_step" class="form-control" required>
           </div>

           <div class="form-group">
               <label>Member Designation</label>
               <input type="text" name="designation" class="form-control" required>
           </div>
           <div class="form-group">
               <label>Left Leg</label>
               <input type="number" name="left_leg" class="form-control" required>
           </div>
           <div class="form-group">
               <label>Right Leg</label>
               <input type="number" name="right_leg" class="form-control" required>
           </div>
           <div class="form-group">
               <label>Incentive Amount</label>
               <input type="text" name="anount" class="form-control" required>
           </div>
       </div>
       <div class="sav-btn">
           <button type="submit">Save Business Plan</button>
       </div>
       <?php echo form_close(); ?>
   </div>