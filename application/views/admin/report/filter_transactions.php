<div class="col-sm-12">
    <?php $this->load->view('admin/includes/_messages'); ?>
</div>
<div class="wrapper2">
    <div class="mblog-post">
        <div class="row">
            <?php echo form_open('admin/filter-transactions') ?>
            <div class="col-md-3">
                <label for="">From</label>
                <div class="form-group">
                    <input type="date" name="form" class="form-control" value="<?php echo $form_date ?>" required>
                </div>
            </div>
            <div class="col-md-3">
                <label for="">To</label>
                <div class="form-group">
                    <input type="date" name="to" class="form-control" value="<?php echo $to_date ?>" required>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-primary"> Filter</button>
            </div>
            <?php echo form_close() ?>
            <div class="col-md-3">
                <a href="<?php echo admin_url() . 'transactions' ?>" class="btn btn-danger"> Reset Filter</a>
            </div>

        </div>
        <div class="row">
            <div class="col-md-4">
                <strong>Incoeme <i class="fa fa-inr" aria-hidden="true"></i>
                    <?php echo html_escape($pl_status['dr']); ?></strong>
            </div>
            <div class="col-md-4">
                <strong>Expenses <i class="fa fa-inr" aria-hidden="true"></i>
                    <?php echo html_escape($pl_status['cr']); ?></strong>
            </div>
            <div class="col-md-4">
                <strong>P/L <i class="fa fa-inr" aria-hidden="true"></i>
                    <?php echo html_escape($pl_status['pl']); ?></strong>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>From</th>
                        <th>Particulars</th>
                        <th>Dr</th>
                        <th>Cr</th>
                        <th>Received On</th>
                        <th>Txn Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($transactions as $item) : ?>
                        <tr>
                            <td><?php echo html_escape($item->id); ?></td>
                            <td><?php echo html_escape(getUsernameById($item->form_user_id)); ?></td>
                            <td><?php echo html_escape($item->perticullars); ?></td>
                            <td><?php echo html_escape($item->dr); ?></td>
                            <td><?php echo html_escape($item->cr); ?></td>
                            <td><?php echo formatted_date($item->created_on); ?></td>
                            <td><?php echo formatted_date($item->tranaction_date); ?></td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>From</th>
                        <th>Particulars</th>
                        <th>Dr</th>
                        <th>Cr</th>
                        <th>Received On</th>
                        <th>Txn Date</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>