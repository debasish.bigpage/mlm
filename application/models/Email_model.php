<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require  APPPATH ."third_party/phpmailer/vendor/autoload.php";
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class Email_model extends CI_Model
{
   //send email reset password
    public function send_email_reset_password($user_id)
    {
        $user_id = clean_number($user_id);
        $user = $this->authication_model->get_user($user_id);
        if (!empty($user)) {
            $token = $user->token;
            //check token
            if (empty($token)) {
                $token = generate_token();
                $data = array(
                    'token' => $token
                );
                $this->db->where('id', $user->id);
                $this->db->update('users', $data);
            }
            $data = array(
                'subject' => "Reset Password",
                'to' => $user->email,
                'template_path' => "email/email_reset_password",
                'token' => $token
            );
            $this->send_email($data);
        }
    }
    //send email newsletter
    public function send_email_notification($data)
    {
            $mailData = array(
                'subject' => $data['subject'],
                'email_content' => $data['email_content'],
                'to' => $data['to'],
                'template_path' => "default/email/email_general",
                'email_button_text' =>  $data['email_button_text'],
                'email_link' =>  $data['email_link'],
                'website_logo'=> $this->getLogo(),
            );
        return $this->send_email($mailData);
    }
    //send email
    public function send_email($data)
    {
      $mail = new PHPMailer(true);
            try {
                $mail->isSMTP();
                $mail->Host = $this->general_settings->mail_host;
                $mail->SMTPAuth = true;
                $mail->SMTPDebug = 0;
                $mail->Username = $this->general_settings->mail_username;
                $mail->Password = $this->general_settings->mail_password;
                $mail->SMTPSecure = 'tls';
                $mail->CharSet = 'UTF-8';
                $mail->Port = $this->general_settings->mail_port;
                //Recipients
                $mail->setFrom($this->general_settings->mail_username, $this->general_settings->mail_title);
                $mail->addAddress($data['to']);
                //Content
                $mail->isHTML(true);
                $mail->Subject = $data['subject'];
                $mail->Body = $this->load->view($data['template_path'], $data, TRUE, 'text/html');
                $mail->send();
                return true;
            } catch (Exception $e) {
                return false;
            }
    }
     public function getEmailSettings()
    {
        $this->db->where('id',1);
        $query = $this->db->get('email_settings');
        $row = $query->row();
        if (!empty($row)) {
           return $row;
        } else {
            return "";
        }
    }

    public function getLogo()
    {
        return  base_url()."assets/frontend/images/logo.png";
    }
}
?>
