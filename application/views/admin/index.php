<div class="wrapper3">

    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="counter">
                <div class="counter-icon">
                    <i class="fa fa-book" aria-hidden="true"></i>
                </div>
                <span class="counter-value"><?php echo $total_member ?></span>
                <h3>Total Member</h3>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="counter red">
                <div class="counter-icon">
                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                </div>
                <span class="counter-value"><?php echo $pendiang_app ?></span>
                <h3>Pending Application</h3>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="counter">
                <div class="counter-icon">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </div>
                <span class="counter-value"><?php echo $rejeted_total ?></span>
                <h3>Rejected Application</h3>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="counter red">
                <div class="counter-icon">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </div>
                <span class="counter-value"><?php echo $tot_ban ?></span>
                <h3>Block Member</h3>
            </div>
        </div>
    </div>


   

    <div class="cus-table">

        <div class="row">
            <div class="col-md-12">
                <div class="panel">

                    <div class="panel-body table-responsive">
                        <div class="table-hed">
                            <h4>Membership Request</h4>

                        </div>
                        <div class="table-part">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>ID</th>
                                       
                                        <th style="width: 13%">Date</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($last_requests as $contacts) : ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $contacts->full_name ?></td>
                                            <td><?php echo $contacts->username ?></td>
                                            <td><?php echo getFormatedDate($contacts->created_on) ?></td>
                                        </tr>
                                    <?php $i++;
                                    endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">

                            <div class="col-sm-12 col-xs-6">
                                <div class="view-btn">
                                    <a href="<?php echo admin_url() . 'contact-messages' ?>">View all</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>