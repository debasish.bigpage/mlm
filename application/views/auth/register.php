 <div class="registration">
     <div class="container">
         <div class="main-form">
             <div class="m-header">
                 <div class="top-header">
                     <h1 class="text-center">ACM</h1>
                     <h5 class="text-center">Marketing pvt.ltd.</h5>
                 </div>
                 <div class="bottom-header pt-3">
                     <div class="row">
                         <div class="col-md-3">
                             <div class="small-part">
                                 <div class="logo">
                                     <img src="<?php echo base_url() ?>assets/images/apicross-logo.png" alt="">
                                 </div>

                             </div>
                         </div>
                         <div class="col-md-6">
                             <div class="smll-content">
                                 <h5>Reg.Office:-127,Simrol Road Gujarkheda</h5>
                                 <h5>Mhow Dist.Indore,(M.P.)</h5>
                                 <h5>Website:-www....</h5>
                                 <h5>Helpline. :- 9912345696</h5>
                                 <h5>Mob. No. :- 9912345696</h5>
                                 <h4>Reg. No.-U667656DFYJTHUSF</h4>
                             </div>
                         </div>
                         <div class="col-md-3">
                             <div class="small-part">
                                 <div class="photo-img">
                                     <img src="<?php echo base_url() ?>assets/images/photo.jpg" alt="">
                                 </div>
                                 <h4>Registration Fee Rs 600/-</h4>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="row ">
                     <div class="col-lg-12 col-md-12 col-xl-12 col-sm-12 mx-auto">
                         <?php $this->load->view('auth/_messages') ?>
                         <div class="registration-form">
                             <h1 class="text-center mb-5">Application Form</h1>
                             <?php echo form_open('save-application'); ?>
                             <div class="row">
                                 <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                                     <label for="">NAME OF APPLICANT</label>
                                     <div class="form-group">
                                         <input type="text" name="full_name" placeholder="ENTER NAME OF APPLICANT" class="form-control" required>
                                     </div>
                                 </div>
                                 <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                                     <label for="">DATE OF BIRTH</label>
                                     <div class="form-group">
                                         <input type="DATE" name="dob" placeholder="ENTER DATE OF BIRTH " class="form-control" required onblur="calculateAge(this.value)">
                                     </div>
                                 </div>
                                 <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                                     <label for="">SEX</label>
                                     <div class="form-group">
                                         <strong>MALE</strong> <input type="radio" name="sex" value="male" required>
                                         <span> <strong> FEMALE </strong><input type="radio" name="sex" value="female"></span>
                                     </div>
                                 </div>
                                 <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                                     <label for="">AGE</label>
                                     <div class="form-group">
                                         <input type="text" readonly name="age" placeholder="ENTER AGE" class="form-control" required>
                                     </div>
                                 </div>

                                 <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                                     <label for="">PAN NO</label>
                                     <div class="form-group">
                                         <input type="text" name="pan" placeholder="ENTER PAN NO" class="form-control" maxlength="10" required>
                                     </div>
                                 </div>

                                 <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                                     <label for="">MOB NO</label>
                                     <div class="form-group">
                                         <input type="text" name="mobile" maxlength="10" placeholder="ENTER MOB NO" class="form-control" required>
                                     </div>
                                 </div>
                                 <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                                     <label for="">EMAIL</label>
                                     <div class="form-group">
                                         <input type="email" name="email" placeholder="ENTER EMAIL" class="form-control" required>
                                     </div>
                                 </div>



                                 <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                                     <label for="">SPO.I.D.NO</label>
                                     <div class="form-group">
                                         <input type="text" name="spon_id" placeholder="ENTER SPO.I.D.NO" class="form-control">
                                     </div>
                                 </div>
                                 <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                                     <label for="">PASSWORD</label>
                                     <div class="form-group">
                                         <input type="password" name="password" placeholder="ENTER PASSWORD" class="form-control" required>
                                     </div>
                                 </div>
                                 <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                                     <label for="">RE-TYPE PASSWORD</label>
                                     <div class="form-group">
                                         <input type="password" name="cpass" placeholder="ENTER PASSWORD" class="form-control" required>
                                     </div>
                                 </div>
                             </div>
                             <h5 class="middle-text text-center my-4">ACCEPTANCE/DECLARATION</h5>
                             <div class="row">

                                 <div class="form-bottom row">

                                     <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 mt-4 text-center">
                                         <p class="text-left mt-3"><input type="checkbox" required /> Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat obcaecati doloribus exercitationem, quam facere error consequatur alias a saepe amet tenetur dolores vel repudiandae est.</p>
                                         <?php generate_recaptcha(); ?>
                                     </div>

                                        

                                 </div>
                             </div>
                             <div class="sub-btn text-center my-5">
                                 <button type="submit" class=""> Submit</button>
                             </div>
                             <?php echo form_close() ?>
                         </div>
                     </div>
                 </div>
             </div>
         </div>


     </div>
 </div>