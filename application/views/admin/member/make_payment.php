<div class="col-sm-12">
    <?php $this->load->view('admin/includes/_messages'); ?>
</div>
<div class="wrapper2">
    <div class="mblog">
        <h4>Application payment receive</h4>
        <?php echo form_open('application_controller/make_payment_post'); ?>
        <div class="form-group">
            <label>Transaction / UTRN Number</label>
            <input type="text" name="utrn" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Amount Received </label>
            <input type="text" name="amount" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Date </label>
            <input type="date" name="trans_date" class="form-control" required>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $app_data->id ?>">
    <div class="sav-btn">
        <button type="submit">Make Payment</button>
    </div>
    <?php echo form_close(); ?>
</div>