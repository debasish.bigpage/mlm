   <div class="col-sm-12">
       <?php $this->load->view('admin/includes/_messages'); ?>
   </div>

   <div class="wrapper2">

       <div class="mblog">

           <h4><?php echo $title ?></h4>
           <?php echo form_open('admin/update-business-plan'); ?>

           <div class="form-group">
               <label>Step Leavel</label>
               <input type="number" name="level_step" class="form-control" required value="<?php echo $member->level_step ?>">
           </div>

           <div class="form-group">
               <label>Member Designation</label>
               <input type="text" name="designation" class="form-control" required value="<?php echo $member->designation ?>">
           </div>
           <div class="form-group">
               <label>Left Leg</label>
               <input type="number" name="left_leg" class="form-control" required value="<?php echo $member->left_leg ?>">
           </div>
           <div class="form-group">
               <label>Right Leg</label>
               <input type="number" name="right_leg" class="form-control" required value="<?php echo $member->right_leg ?>">
           </div>
           <div class="form-group">
               <label>Incentive Amount</label>
               <input type="text" name="anount" class="form-control" required value="<?php echo $member->anount ?>">
           </div>
       </div>
       <div class="sav-btn">
           <input type="hidden" name="id" value="<?php echo $member->id ?>">
           <button type="submit">Update Business Plan</button>
       </div>
       <?php echo form_close(); ?>
   </div>