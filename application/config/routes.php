<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Auth_controller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['login']             = 'auth_controller/index';
$route['login-check']       = 'auth_controller/login_post';
$route['logout']            = 'auth_controller/logout';
$route['register']            = 'auth_controller/register';
$route['save-application']            = 'auth_controller/register_post';
$route['registration-compleate/(:any)']  = 'auth_controller/reg_thank_you/$1';
$route['change-password']   = 'auth_controller/change_password';
$route['update-password']   = 'auth_controller/change_password_post';
$route['reset-password']    = 'auth_controller/reset_password';
$route['forgot-password'] = 'auth_controller/forgot_password';
$route['check-forgot-password'] = 'auth_controller/forgot_password_post';
$route['view-member/(:num)'] = 'member_controller/view_profile/$1';
/*
 *
 * CRON JOBS
 * 
 */
$route['cron-jobs'] = 'cron_controller/index';
//monthly cashback
$route['cashback-cron-jobs'] = 'cron_controller/setCashback';


// member area
$route['member-portal'] = 'member_controller/index';
$route['change-profile'] = 'member_controller/profile';
$route['save-profile'] = 'member_controller/profileUpdatePost';

/*
 *
 * ADMIN ROUTES
 *
 */
$route['admin']         = 'admin/index';
$route['admin/dashboard']         = 'admin/index';
$route['admin/email-settings']    = 'admin/emailSettings';
$route['admin/pagination-settings']    = 'admin/paginationSettings';
$route['admin/settings']    = 'admin/settings';
$route['admin/visual-settings'] = 'admin/visual_settings';
$route['admin/bank-settings'] = 'admin/bank_settings';
$route['admin/languages'] = 'language_controller/languages';
$route['admin/change-profile'] = 'admin/change_profile';
$route['admin/save-profile'] = 'admin/save_profile';
$route['admin/pending-application'] = 'application_controller/pending';
$route['admin/rejected-application'] = 'application_controller/reject';
$route['admin/view-application/(:num)'] = 'application_controller/view_application/$1';
// business plan settings
$route['admin/business-settings'] = 'admin/business_settings';
$route['admin/add-business-plan'] = 'admin/add_business_plan';
$route['admin/save-business-plan'] = 'admin/add_business_plan_post';
$route['admin/edit-business-plan/(:num)'] = 'admin/edit_business_plan/$1';
$route['admin/update-business-plan'] = 'admin/edit_business_plan_post';
// reward plan settings
$route['admin/comapny-reward-settings'] = 'admin/reward_settings';
$route['admin/add-reward-plan'] = 'admin/add_reward_plan';
$route['admin/save-reward-plan'] = 'admin/add_reward_plan_post';
$route['admin/edit-reward-plan/(:num)'] = 'admin/edit_reward_plan/$1';
$route['admin/update-reward-plan'] = 'admin/edit_reward_plan_post';
// cashback settings
$route['admin/cashback-settings'] = 'admin/cashback_settings';
$route['admin/save-cashback-settings'] = 'admin/cashback_settings_post';
//network accept
$route['admin/make-payment/(:num)'] = 'application_controller/make_payment/$1';
$route['admin/approve-member/(:num)'] = 'application_controller/approve_member/$1';
$route['admin/reject-member/(:num)'] = 'application_controller/reject_member/$1';
//report Sections
$route['admin/transactions'] = 'admin/transactions';
$route['admin/filter-transactions'] = 'admin/filter_transactions';
$route['admin/payout-report'] = 'admin/payouts';
$route['admin/filter-payouts'] = 'admin/filter_payout';
$route['admin/pending-payout-report'] = 'admin/pending_payouts';
$route['admin/release-all-payouts'] = 'admin/release_bulk_payout';
$route['admin/release-payout/(:num)'] = 'admin/release_payout/$1';

$route['admin/reward-report'] = 'admin/rewards';
$route['admin/filter-reward'] = 'admin/filter_reward';
$route['admin/pending-reward-report'] = 'admin/pending_reward';
$route['admin/release-all-reward'] = 'admin/release_bulk_reward';
$route['admin/release-reward/(:num)'] = 'admin/release_reward/$1';

$route['admin/cashback-report'] = 'admin/cashbacks';
$route['admin/filter-cashback'] = 'admin/filter_cashback';
$route['admin/pending-cashback-report'] = 'admin/pending_cashback';
$route['admin/release-all-cashback'] = 'admin/release_bulk_cashback';
$route['admin/release-cashback/(:num)'] = 'admin/release_cashback/$1';

$route['admin/mlm-users'] = 'admin/mlm_user';
$route['admin/edit-user-status/(:num)'] = 'admin/edit_user_status/$1';
$route['admin/tree-view/(:num)'] = 'admin/generology/$1';