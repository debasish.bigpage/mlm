<html lang="EN">

<head>
  <meta charset="UTF-8">
  <link rel="shortcut icon" type="<?php echo base_url() . 'uploads/logo/' . $this->general_settings->favicon ?>" href="<?php echo base_url() . 'uploads/logo/' . $this->general_settings->favicon ?>">
  <title><?php echo xss_clean($title); ?> - <?php echo xss_clean($this->settings->site_title); ?></title>
  <link rel="icon" href="<?php echo base_url(); ?>assets/frontend/images/amma_logo_animated_ver4.gif" type="image/x-icon">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/css/responsive.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/css/style.css'); ?>">
  <!-- Datatables -->
  <link href="<?php echo base_url('assets/backend/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/datatables/buttons.dataTables.min.css'); ?>">
  <script>
    var base_url = '<?php echo base_url(); ?>';
    var csfr_token_name = '<?php echo $this->security->get_csrf_token_name(); ?>';
    var csfr_cookie_name = '<?php echo $this->config->item('csrf_cookie_name'); ?>';
  </script>
</head>

<body>
  <aside class="mside-nav" id="show-mside-navigation1">
    <i class="fa fa-bars close-aside hidden-sm hidden-md hidden-lg" data-close="show-mside-navigation1"></i>
    <div class="heading">
      <img src="<?php echo base_url('assets/backend/images/man.jpg'); ?>" alt="">
      <div class="info">
        <h3><a href="<?php echo base_url('dashboard') ?>"><?php echo ($this->session->userdata('bp_sess_usertype') == "admin") ? "Administrator" : "Member" ?></a></h3>
        <p>Active</p>
      </div>
    </div>
    <!--  <div class="search">
      <input type="text" placeholder="Type here"><i class="fa fa-search"></i>
    </div> -->
    <ul class="categories">
      <li class="f-list"><i class="fa fa-tachometer" aria-hidden="true"></i><a href="<?php echo base_url('dashboard') ?>">Dashboard</a>

     <!--  <li><i class="fa fa-picture-o fa-fw"></i><a href="#">Slider / Banner</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() ?>sliders">Slider</a></li>
          <li><a href="<?php echo admin_url() ?>banner">Banner</a></li>
        </ul>
      </li>
      <li><i class="fa fa-home fa-fw" aria-hidden="true"></i><a href="#">Home Page</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() ?>about-us">About Us</a></li>
          <li><a href="<?php echo admin_url() ?>activities">Our Activities</a></li>
          <li><a href="<?php echo admin_url() ?>future-prospect">Future Prospect</a></li>
          <li><a href="<?php echo admin_url() ?>project">Our Project</a></li>
          <li><a href="<?php echo admin_url() ?>help">Help</a></li>
          <li><a href="<?php echo admin_url() ?>mission">Mission Vision</a></li>
          <li><a href="<?php echo admin_url() ?>team">Our Team</a></li>
          <li><a href="<?php echo admin_url() ?>message">President Message</a></li>
          <li><a href="<?php echo admin_url() ?>home-page-settings">Home Page Settings</a></li>
        </ul>
      </li>
      <li class="f-list"><i class="fa fa-briefcase" aria-hidden="true"></i><a href="<?php echo admin_url() . 'all-activities' ?>">Activities</a>
      <li class="f-list"><i class="fa fa-sitemap" aria-hidden="true"></i><a href="<?php echo admin_url() . 'all-projects' ?>">Projects</a>
      <li><i class="fa fa-users fa-fw"></i><a href="#">Govt. Schemes</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() ?>scheme-categories">Scheme Category</a></li>
          <li><a href="<?php echo admin_url() ?>all-scheme">All Schemes</a></li>
        </ul>
      </li>
      <li class="f-list"><i class="fa fa-child" aria-hidden="true"></i><a href="<?php echo admin_url() . 'all-campaign' ?>">National Campaign</a></li>
      <li><i class="fa fa-file fa-fw"></i><a href="#"> CMS Page</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() ?>all-pages">All Pages</a></li>
          <li><a href="<?php echo admin_url() ?>add-page">Add Page</a></li>
        </ul>
      </li>
      <li><i class="fa fa-photo fa-fw"></i><a href="#"> Gallery</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() ?>image-gallery">Image Gallery</a></li>
          <li><a href="<?php echo admin_url() ?>report-gallery">Reports Gallery</a></li>
          <li><a href="<?php echo admin_url() ?>media-gallery">Media Gallery</a></li>
        </ul>
      </li>
      <li><i class="fa fa-handshake-o fa-fw"></i><a href="#"> Join Us Settings</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() ?>organisation-member-settings">Organisation Member</a></li>
          <li><a href="<?php echo admin_url() ?>volunteer-member-settings">Volunteer Member</a></li>
        </ul>
      </li>
      <li><i class="fa fa-envira fa-fw"></i><a href="#"> Join Us Request</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() ?>organisation-member-request">Organisation Member</a></li>
          <li><a href="<?php echo admin_url() ?>volunteer-member-request">Volunteer Member</a></li>
        </ul>
      </li>
      <li><i class="fa fa-users fa-fw"></i><a href="#">Coordinators Team</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() ?>all-member">Team Member</a></li>
          <li><a href="<?php echo admin_url() ?>add-member">Add Member</a></li>

        </ul>
      </li>
      <li><i class="fa fa-users fa-fw"></i><a href="#">Administration Team</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() ?>all-administration-member">Team Member</a></li>
          <li><a href="<?php echo admin_url() ?>add-administration-member">Add Member</a></li>

        </ul>
      </li>
      <li class="f-list"><i class="fa fa-paper-plane" aria-hidden="true"></i><a href="<?php echo admin_url() . 'contact-messages' ?>">Contact messages</a>
      <li><i class="fa fa-bolt fa-fw"></i><a href="#">Settings</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() . 'settings'; ?>"> General Setting </a></li>
          <li><a href="<?php echo admin_url() . 'email-settings'; ?>">Email Settings</a></li>
          <li><a href="<?php echo admin_url() . 'bank-settings'; ?>"> Bank Setting </a></li>
          <li><a href="<?php echo admin_url() . 'visual-settings'; ?>">Visual Settings</a></li>
          <li><a href="<?php echo admin_url() . 'pagination-settings'; ?>">pagination Settings</a></li>
        </ul>
      </li> -->

    </ul>
  </aside>
  <section id="contents" style="padding: 0;">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <i class="fa fa-align-right"></i>
          </button>
          <a class="navbar-brand" href="#"><?php echo $title ?></a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url('change-profile'); ?>"><i class="fa fa-user"></i> Change profile</a></li>
                <li><a href="<?php echo base_url('change-password'); ?>"><i class="fa fa-lock"></i> Change Password</a></li>
                <li><a href="<?php echo base_url('logout'); ?>"><i class="fa fa-sign-out"></i> Log out</a></li>
              </ul>
            </li>
            <li style="padding-top: 17px;"><a href="#"><i data-show="show-mside-navigation1" class="fa fa-bars show-side-btn"></i></a></li>
          </ul>
        </div>
      </div>
    </nav>