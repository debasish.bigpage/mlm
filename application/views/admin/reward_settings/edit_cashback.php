   <div class="col-sm-12">
       <?php $this->load->view('admin/includes/_messages'); ?>
   </div>

   <div class="wrapper2">

       <div class="mblog">

           <h4><?php echo $title ?></h4>
           <?php echo form_open('admin/save-cashback-settings'); ?>
           <div class="form-group">
               <label>Total Member</label>
               <input type="number" name="total_member" class="form-control" required value="<?php echo $cashback_settings->total_member ?>">
           </div>

           <div class="form-group">
               <label>Cashback Amount</label>
               <input type="text" name="amount" class="form-control" required value="<?php echo $cashback_settings->amount ?>">
           </div>
       </div>
       <div class="sav-btn">
           <button type="submit">Save Reward Plan</button>
       </div>
       <?php echo form_close(); ?>
   </div>