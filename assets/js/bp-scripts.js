//token update
$("form").submit(function () {
    $("input[name='" + csfr_token_name + "']").val($.cookie(csfr_cookie_name));
});


function calculateAge(dob) {
    var data = {
        "dob": dob
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);
    $.ajax({
        type: "POST",
        url: base_url + "ajax_controller/claculateAge",
        data: data,
        success: function (response) {
            $('input[name="age"]').val(response);
        }
    });
}