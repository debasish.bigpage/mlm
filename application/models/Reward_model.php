<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reward_model extends CI_Model
{
    protected function input_values()
    {
        $data = array(
            'level_step' => $this->input->post('level_step', true),
            'timeline' => $this->input->post('timeline', true),
            'reward_name' => $this->input->post('reward_name', true),
            'price' => $this->input->post('price', true),
        );
        return $data;
    }

    public function get_all_reward_plans($where = '')
    {
        if (!empty($where)) {
            $this->bp->where($where);
        }
        $query = $this->db->get('company_reward_settings');
        return $query->result();
    }

    public function add_reward_plan()
    {
        $data = $this->input_values();
        return $this->db->insert('company_reward_settings', $data);
    }

    //delete contact message
    public function delete_reward_plan($id)
    {
        $id = clean_number($id);
        $this->db->where('id', $id);
        return $this->db->delete('company_reward_settings');
    }

    //get project by id
    public function get_reward_plan_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('company_reward_settings');
        return $query->row();
    }

    public function update($id)
    {
        //set values
        $data = $this->input_values();
        $project = $this->get_reward_plan_by_id($id);
        if (!empty($project)) {
            $this->db->where('id', $id);
            return $this->db->update('company_reward_settings', $data);
        }
        return false;
    }

    
}
