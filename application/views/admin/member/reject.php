<div class="wrapper2">

    <?php $this->load->view('admin/includes/_messages'); ?>

    <div class="mblog-post">

        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>UserID</th>
                        <th>Name</th>

                        <th>Cause</th>
                        <th>Option</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($applications as $item) : ?>
                        <tr>
                            <td><?php echo html_escape($item->id); ?></td>
                            <td><?php echo html_escape($item->username); ?></td>
                            <td><?php echo html_escape($item->full_name); ?></td>
                            <td><?php echo html_escape($item->rejection_cause); ?></td>
                            <td class="drp-btn">
                                <div class="dropdown drp">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select a Option
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="<?php echo admin_url() . 'view-application/' . $item->id ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View </a>

                                        <a class="dropdown-item" href="javascript:void(0)" onclick="delete_item('application_controller/delete_application','<?php echo $item->id; ?>','Are you want to delete this application?');"><i class="fa fa-trash" aria-hidden="true"></i> Delete </a>
                                    </div>
                                </div>
                            </td>
                        </tr>

                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>UserID</th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Option</th>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>