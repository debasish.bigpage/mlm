<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model
{
    //update settings
    public function update_settings()
    {
        $data = array(
            'site_title' => $this->input->post('site_title', true),
            'homepage_title' => $this->input->post('homepage_title', true),
            'site_description' => $this->input->post('site_description', true),
            'keywords' => $this->input->post('keywords', true),
            'facebook_url' => $this->input->post('facebook_url', true),
            'twitter_url' => $this->input->post('twitter_url', true),
            'instagram_url' => $this->input->post('instagram_url', true),
            'linkedin_url' => $this->input->post('linkedin_url', true),
            'youtube_url' => $this->input->post('youtube_url', true),
            'contact_text' => $this->input->post('contact_text', false),
            'contact_address' => $this->input->post('contact_address', true),
            'contact_email' => $this->input->post('contact_email', true),
            'contact_phone' => $this->input->post('contact_phone', true),
            'about_us' => $this->input->post('about_us', true),
            'map' => $this->input->post('map'),
        );
       

         $this->db->where('id', 1);
        return $this->db->update('settings', $data);
    }

    //update general settings
    public function update_general_settings()
    {
        $data = array(
            'application_name' => $this->input->post('application_name', true),
            'custom_css_codes' => $this->input->post('custom_css_codes', false),
            'custom_javascript_codes' => $this->input->post('custom_javascript_codes', false),
        );
        $this->db->where('id', 1);
        return $this->db->update('general_settings', $data);
    }

    //update recaptcha settings
    public function update_recaptcha_settings()
    {
        $data = array(
            'recaptcha_site_key' => $this->input->post('recaptcha_site_key', true),
            'recaptcha_secret_key' => $this->input->post('recaptcha_secret_key', true),
            'recaptcha_lang' => $this->input->post('recaptcha_lang', true),
        );
        $this->db->where('id', 1);
        return $this->db->update('general_settings', $data);

    }

    //update email settings
    public function update_email_settings()
    {
        $data = array(
            'mail_protocol' => $this->input->post('mail_protocol', true),
            'mail_title' => $this->input->post('mail_title', true),
            'mail_host' => $this->input->post('mail_host', true),
            'mail_port' => $this->input->post('mail_port', true),
            'mail_username' => $this->input->post('mail_username', true),
            'mail_password' => $this->input->post('mail_password', true),
        );

        $this->db->where('id', 1);
        return $this->db->update('general_settings', $data);
    }

    //update email verification
    public function update_email_verification()
    {
        $data = array(
            'email_verification' => $this->input->post('email_verification', true),
        );

        $this->db->where('id', 1);
        return $this->db->update('general_settings', $data);
    }


    //update email settings
    public function update_pagination_settings()
    {
        $data = array(
            'item_per_page' => $this->input->post('item_per_page', true),
        );

        $this->db->where('id', 1);
        return $this->db->update('general_settings', $data);
    }

  



    //update visual settings
    public function update_visual_settings()
    {
        $this->load->library('upload');
        $data = array(
        );
        // upload logo
        if ($_FILES['logo']['name'] != '') {
            $path = FCPATH . 'uploads/logo/';
            if (!is_dir($path)) {
                mkdir($path, 0755, TRUE);
            }
            $config_prof['encrypt_name']  = TRUE;
            $config_prof['upload_path']   = $path;
            $config_prof['allowed_types'] = 'png|jpeg|jpg';
            $config['max_size']           = '5000';
            $config_prof['overwrite']     = true;
            $this->upload->initialize($config_prof);
            if ($this->upload->do_upload('logo')) {
                $file_data = $this->upload->data();
                $data["logo"] = $file_data['file_name'];
                $data["logo_email"] = $file_data['file_name'];
            } else {
                return false;
            }
            
        }
        // upload favicon
        if ($_FILES['favicon']['name'] != '') {
            $path = FCPATH . 'uploads/logo/';
            if (!is_dir($path)) {
                mkdir($path, 0755, TRUE);
            }
            $config_prof['encrypt_name']  = TRUE;
            $config_prof['upload_path']   = $path;
            $config_prof['allowed_types'] = 'png|jpeg|jpg';
            $config['max_size']           = '5000';
            $config_prof['overwrite']     = true;
            $this->upload->initialize($config_prof);
            if ($this->upload->do_upload('favicon')) {
                $file_data = $this->upload->data();
                $data["favicon"] = $file_data['file_name'];
            } else {
                return false;
            }
        }
        $this->db->where('id', 1);
        return $this->db->update('general_settings', $data);
    }

    /*
    * UPLoad Section
    *
    */
    //logo image upload
    public function logo_upload($file_name)
    {
        print_r($_FILES);exit;
        $config['upload_path'] = FCPATH . 'uploads/logo/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|svg';
        $config['file_name'] = 'logo_' . uniqid();
        $this->load->library('upload', $config);

        if ($this->upload->do_upload($file_name)) {
            $data = array('upload_data' => $this->upload->data());
            if (isset($data['upload_data']['full_path'])) {
                return 'uploads/logo/' . $data['upload_data']['file_name'];
            }
        }
        return null;
    }

    //favicon image upload
    public function favicon_upload($file_name)
    {
        $config['upload_path'] = FCPATH . 'uploads/logo/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['file_name'] = 'favicon_' . uniqid();
        $this->load->library('upload', $config);

        if ($this->upload->do_upload($file_name)) {
            $data = array('upload_data' => $this->upload->data());
            if (isset($data['upload_data']['full_path'])) {
                return 'uploads/logo/' . $data['upload_data']['file_name'];
            }
        }
        return null;
    }

    

   
    //get general settings
    public function get_general_settings()
    {
        $this->db->where('id', 1);
        $query = $this->db->get('general_settings');
        return $query->row();
    }

    //get system settings
    public function get_system_settings()
    {
        $this->db->where('id', 1);
        $query = $this->db->get('general_settings');
        return $query->row();
    }

  

    //get panel settings
    public function get_panel_settings()
    {
        //return @get_user_session();
    }

    //get index settings
    public function get_index_settings()
    {
        //return @get_current_user_session();
    }

    //get settings
    public function get_settings()
    {
         $this->db->where('id', 1);
        $query = $this->db->get('settings');
        return $query->row();
    }

    //get routes
    public function get_routes()
    {
        $query = $this->db->query("SELECT * FROM routes WHERE id = 1");
        return $query->row();
    }

    public function get_bank_settings()
    {
        $this->db->where('id', 1);
        $query = $this->db->get('bank_settings');
        return $query->row();
    }

    public function update_bank_settings()
    {
        $data = array(
            'account_name' => $this->input->post('account_name', true),
            'bank_name' => $this->input->post('bank_name', true),
            'account_no' => $this->input->post('account_no', true),
            'ifsc_code' => $this->input->post('ifsc_code', true),
            'branch_address' => $this->input->post('branch_address', true),
        );
        
        $this->db->where('id', 1);
        return $this->db->update('bank_settings', $data);
    }


  
    

  

  
}
