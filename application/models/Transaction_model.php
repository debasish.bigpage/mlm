<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Transaction_model extends CI_Model
{
    /*
    *-------------------------------------------------------------------------------------------------
    * Tranasaction Register
    *-------------------------------------------------------------------------------------------------
    */
    public function get_all_trannsactions($where = '')
    {
        if (!empty($where)) {
            $this->bp->where($where);
        }
        $this->db->order_by("id","DESC");
        $query = $this->db->get('transaction');
        return $query->result();
    }

    public function add_transaction($data)
    {
        return $this->db->insert('transaction', $data);
    }

    
    //get project by id
    public function get_trannsaction_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('transaction');
        return $query->row();
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * Application FEE
    *-------------------------------------------------------------------------------------------------
    */
    public function get_all_app_fee($where = '')
    {
        if (!empty($where)) {
            $this->bp->where($where);
        }
        $query = $this->db->get('application_fees_trans');
        return $query->result();
    }

    public function add_app_fee($data)
    {
        return $this->db->insert('application_fees_trans', $data);
    }


    //get project by id
    public function get_app_fee_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('application_fees_trans');
        return $query->row();
    }

    public function getTnsPL($where="")
    {
        $sqlTd = "SELECT SUM(dr) AS dr FROM transaction";
        if(!empty($where)){
            $sqlTd .= " WHERE ".$where;
        }
        
        $res = $this->db->query($sqlTd)->row();
        $tdr = $res->dr;
        $data['dr']=$tdr;
        $sqlTc = "SELECT SUM(cr) AS cr FROM transaction";
        $res2 = $this->db->query($sqlTc)->row();
        $tcr = $res2->cr;
        $data['cr'] = $tcr;
        $data['pl'] = number_format($tdr-$tcr,2);
        return $data;
        
    }


}
?>