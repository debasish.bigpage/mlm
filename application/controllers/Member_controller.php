<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member_controller extends Home_Core_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!auth_check()) {
            redirect('login');
        }
        $this->load->library('upload');
    }

    public function index()
    {
        $data['user_data']= user();
        $data['bank_settings'] = $this->settings_model->get_bank_settings();
        $data['title'] = "Member Panel";
        $this->load->view('member/includes/header', $data);
        if($data["user_data"]->is_approved==1){
           
            $this->load->view('member/dashboard', $data);
        }else{
            $this->load->view('member/tempDashboard', $data);
        }
        $this->load->view('member/includes/footer');
    }

    public function profile()
    {
        $data['user_data']= user();
        $data['title'] = "Profile";
        $this->load->view('member/includes/header', $data);
        $this->load->view('member/profile', $data);
        $this->load->view('member/includes/footer');

    }

    public function profileUpdatePost()
    {
        $oldIng = $this->input->post('oldIng');
        $oldpan = $this->input->post('oldpan');
        $oldsign = $this->input->post('oldsign');
        $this->form_validation->set_rules('father_name', "Father's/husband's Name", 'required|xss_clean|min_length[4]|max_length[250]');
        $this->form_validation->set_rules('sex', "Sex", 'required|xss_clean');
        $this->form_validation->set_rules('occupation', "Occupation", 'required|xss_clean|min_length[4]|max_length[255]');
        $this->form_validation->set_rules('address', "Address", 'required|xss_clean');
        $this->form_validation->set_rules('state', "State", 'required|xss_clean');
        $this->form_validation->set_rules('pin', "PIN Code", 'required|xss_clean|min_length[6]|max_length[6]|numeric');
        $this->form_validation->set_rules('phone', "Phone Number", 'required|xss_clean|min_length[10]|max_length[11]|numeric');
        $this->form_validation->set_rules('bank_name', "Bank Name", 'required|xss_clean');
        $this->form_validation->set_rules('acno', "Account Number", 'required|xss_clean|numeric');
        $this->form_validation->set_rules('bank_ifsc', "Bank IFSC Code", 'required|xss_clean');
        $this->form_validation->set_rules('nomine_name', "Nomine Name", 'required|xss_clean');
        $this->form_validation->set_rules('relation', "Relation", 'required|xss_clean');
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            $data = array(
                'father_name'=>$this->input->post('father_name',true),
                'sex'=>$this->input->post('sex',true),
                'occupation'=>$this->input->post('occupation',true),
                'address'=>$this->input->post('address',true),
                'state'=>$this->input->post('state',true),
                'pin'=>$this->input->post('pin',true),
                'phone'=>$this->input->post('phone',true),
                'bank_name'=>$this->input->post('bank_name',true),
                'acno'=>$this->input->post('acno',true),
                'bank_ifsc'=>$this->input->post('bank_ifsc',true),
                'nomine_name'=>$this->input->post('nomine_name',true),
                'relation'=>$this->input->post('relation',true),
            );
            if ($_FILES['avatar']['name'] != '') {
                $path = FCPATH . 'uploads/member-doc/';
                if (!is_dir($path)) {
                    mkdir($path, 0755, TRUE);
                }
                $config_prof['encrypt_name']  = TRUE;
                $config_prof['upload_path']   = $path;
                $config_prof['allowed_types'] = 'png|jpeg|jpg';
                $config['max_size']           = '5000';
                $config_prof['overwrite']     = true;
                $this->upload->initialize($config_prof);
                if ($this->upload->do_upload('avatar')) {
                    $file_data = $this->upload->data();
                    $this->resizeImage($file_data);
                    $data["avatar"] =  $file_data['file_name'];
                    if (!empty($oldIng)) {
                        delete_file_from_server('uploads/member-doc/' . $oldIng);
                    }
                } else {
                    $this->session->set_flashdata('error',  $this->upload->display_errors());
                    redirect($this->agent->referrer());
                }
            }
            if ($_FILES['pan_img']['name'] != '') {
                $path = FCPATH . 'uploads/member-doc/';
                if (!is_dir($path)) {
                    mkdir($path, 0755, TRUE);
                }
                $config_prof['encrypt_name']  = TRUE;
                $config_prof['upload_path']   = $path;
                $config_prof['allowed_types'] = 'png|jpeg|jpg';
                $config['max_size']           = '5000';
                $config_prof['overwrite']     = true;
                $this->upload->initialize($config_prof);
                if ($this->upload->do_upload('pan_img')) {
                    $file_data = $this->upload->data();
                    $this->resizeImage($file_data);
                    $data["pan_img"] =  $file_data['file_name'];
                    if (!empty($oldpan)) {
                        delete_file_from_server('uploads/member-doc/' . $oldpan);
                    }
                } else {
                    $this->session->set_flashdata('error',  $this->upload->display_errors());
                    redirect($this->agent->referrer());
                }
            }
            if ($_FILES['sign_img']['name'] != '') {
                $path = FCPATH . 'uploads/member-doc/';
                if (!is_dir($path)) {
                    mkdir($path, 0755, TRUE);
                }
                $config_prof['encrypt_name']  = TRUE;
                $config_prof['upload_path']   = $path;
                $config_prof['allowed_types'] = 'png|jpeg|jpg';
                $config['max_size']           = '5000';
                $config_prof['overwrite']     = true;
                $this->upload->initialize($config_prof);
                if ($this->upload->do_upload('sign_img')) {
                    $file_data = $this->upload->data();
                    $this->resizeImage($file_data);
                    $data["sign_img"] =  $file_data['file_name'];
                    if (!empty($oldsign)) {
                        delete_file_from_server('uploads/member-doc/' . $oldsign);
                    }
                } else {
                    $this->session->set_flashdata('error',  $this->upload->display_errors());
                    redirect($this->agent->referrer());
                }
            }
            if ($this->authication_model->updateUsers($data, $this->session->userdata('bp_sess_user_id'))) {
                $this->session->set_flashdata('success', "Profile Updated");
                redirect($this->agent->referrer());
            } else {
                
                $this->session->set_flashdata('error', "Unable update profile!");
                redirect($this->agent->referrer());
            }

        }
    }

    public function resizeImage($file_data)
    {
        $this->load->library('image_lib');
        $path = FCPATH . "/uploads/member-doc/";
        $height = 480;
        $width = 640;
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_data["raw_name"] . $file_data['file_ext'];
        $config['new_image'] = $path . $file_data["raw_name"] . $file_data['file_ext'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = "60";
        $config['width']         = $width;
        $config['height']       = $height;
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    public function view_profile($id)
    {
        $id = clean_number($id);
        $data['user_data'] = $this->authication_model->get_user($id);
        $data['title'] = "Member Details";
        if (is_admin()) {
            $this->load->view('admin/includes/header', $data);
            $this->load->view('member/view_full_profile', $data);
            $this->load->view('admin/includes/footer');
        }else{
            $this->load->view('member/includes/header', $data);
            $this->load->view('member/view_full_profile', $data);
            $this->load->view('member/includes/footer');
        }
        
    }
}