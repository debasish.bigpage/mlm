<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cashback_model extends CI_Model
{
    protected function input_values()
    {
        $data = array(
            'total_member' => $this->input->post('total_member', true),
            'amount' => $this->input->post('amount', true),
        );
        return $data;
    }

    
    //get project by id
    public function get_cashback_settings()
    {
        $this->db->where('id', 1);
        $query = $this->db->get('cashback_settings');
        return $query->row();
    }

    public function update()
    {
        $data = $this->input_values();
        $this->db->where('id', 1);
        return $this->db->update('cashback_settings', $data);
        
    }

    /*
    *Cashback user entry
    * 
    */
    public function insert_cashback_urser($data)
    {
        return $this->db->insert('cashback_user_list', $data);
    }

    public function get_all_cashback_user($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by('id', "DESC");
        $query = $this->db->get('cashback_user_list');
        return $query->result();
    }
    //get project by id
    public function get_cashback_user_by_id($id)
    {
        $this->db->where('user_id', $id);
        $query = $this->db->get('cashback_user_list');
        return $query->row();
    }



   

}
