<div class="wrapper2">
    <?php $this->load->view('admin/includes/_messages'); ?>
    <div class="mblog-post">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Full Name</th>
                    <td><?php echo $user_data->full_name ?></td>
                    <th>Registration NO</th>
                    <td><?php echo $user_data->username ?></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><?php echo $user_data->email ?></td>
                    <th>Mobile NO</th>
                    <td><?php echo $user_data->mobile ?></td>
                </tr>
                <tr>
                    <th>Date Of Birth</th>
                    <td><?php echo date("d-m-Y",strtotime($user_data->dob)) ?></td>
                    <th>Gender</th>
                    <td><?php echo $user_data->sex ?></td>
                </tr>
                <tr>
                    <th>PAN</th>
                    <td><?php echo $user_data->pan ?></td>
                    <th>Sponser By</th>
                    <td><?php echo getUsernameById($user_data->spon_id) ?></td>
                </tr>
                <tr>
                    <th>Father Name</th>
                    <td><?php echo $user_data->father_name ?></td>
                    <th>Occupation</th>
                    <td><?php echo $user_data->occupation ?></td>
                </tr>
                <tr>
                    <th>Address</th>
                    <td><?php echo $user_data->address ?></td>
                    <th>State</th>
                    <td><?php echo $user_data->state ?></td>
                </tr>
                <tr>
                    <th>PIN </th>
                    <td><?php echo $user_data->pin ?></td>
                    <th>Phone</th>
                    <td><?php echo $user_data->phone ?></td>
                </tr>
                <tr>
                    <th>Bank Name</th>
                    <td><?php echo $user_data->bank_name ?></td>
                    <th>A/C No</th>
                    <td><?php echo $user_data->acno ?></td>
                </tr>
                <tr>
                    <th>Bank Name</th>
                    <td><?php echo $user_data->bank_name ?></td>
                    <th>A/C No</th>
                    <td><?php echo $user_data->acno ?></td>
                </tr>
                <tr>
                    <th>IFSC Code</th>
                    <td><?php echo $user_data->bank_ifsc ?></td>
                    <th>Nomini Name</th>
                    <td><?php echo $user_data->nomine_name ?></td>
                </tr>
                <tr>
                    <th>Nomini Relation</th>
                    <td><?php echo $user_data->relation ?></td>
                    <th>Degignation</th>
                    <td><?php echo $user_data->degignation ?></td>
                </tr>
                <tr>
                    <th>Created On</th>
                    <td><?php echo formatted_date($user_data->created_on) ?></td>
                    <th>Approved On</th>
                    <td><?php echo formatted_date($user_data->approved_on) ?></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table width="100%">
                            <tr>
                                <td>
                                    <?php if (!empty($item->avatar)) : ?>
                                        <img src="<?php echo base_url() . 'uploads/product/' . $item->avatar; ?>" alt="" />
                                    <?php else : ?>
                                        <img src="<?php echo base_url('assets/backend/images/no-user.png') ?>" alt="" />
                                    <?php endif; ?>
                                    <p class="text-center">Photo</p>
                                </td>
                                <td>
                                    <?php if (!empty($item->sign_img)) : ?>
                                        <img src="<?php echo base_url() . 'uploads/product/' . $item->sign_img; ?>" alt="" />
                                    <?php else : ?>
                                        <img src="<?php echo base_url('assets/backend/images/noimg.png') ?>" alt="" />
                                    <?php endif; ?>
                                    <p class="text-center">Signature</p>
                                </td>
                                <td>
                                    <?php if (!empty($item->pan_img)) : ?>
                                        <img src="<?php echo base_url() . 'uploads/product/' . $item->pan_img; ?>" alt="" />
                                    <?php else : ?>
                                        <img src="<?php echo base_url('assets/backend/images/noimg.png') ?>" alt="" />
                                    <?php endif; ?>
                                    <p class="text-center">PAN Card</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>