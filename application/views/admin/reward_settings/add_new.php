   <div class="col-sm-12">
       <?php $this->load->view('admin/includes/_messages'); ?>
   </div>

   <div class="wrapper2">

       <div class="mblog">

           <h4><?php echo $title ?></h4>
           <?php echo form_open('admin/save-reward-plan'); ?>
           <div class="form-group">
               <label>Step Leavel</label>
               <select name="level_step" class="form-control" required>
                   <option value="">select One </option>
                   <?php foreach ($business_settings as $steps) : ?>
                       <option value="<?php echo $steps->id; ?>"><?php echo $steps->designation . ' - ' . $steps->id; ?></option>
                   <?php endforeach; ?>
               </select>
           </div>

           <div class="form-group">
               <label>Time (In Days)</label>
               <input type="number" name="timeline" class="form-control" required>
           </div>
           <div class="form-group">
               <label>Reward Name</label>
               <input type="text" name="reward_name" class="form-control" required>
           </div>
           <div class="form-group">
               <label>Price Amount</label>
               <input type="text" name="price" class="form-control" required>
           </div>
       </div>
       <div class="sav-btn">
           <button type="submit">Save Reward Plan</button>
       </div>
       <?php echo form_close(); ?>
   </div>