<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Admin extends Admin_Core_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!auth_check()) {
            redirect('login');
        }
        if (!is_admin()) {
            redirect('dashboard');
        }
    }
    public function index()
    {
        $data['title'] = "Admin Panel";
        $data['total_member'] = $this->authication_model->get_cond_users_count();
        $data['pendiang_app'] = $this->authication_model->get_cond_users_count(array('is_approved' => 0));
        $data['rejeted_total'] = $this->authication_model->get_cond_users_count(['is_approved' => 2]);
        $data['tot_ban'] = $this->authication_model->get_cond_users_count(['banned'=>1]);
        $data['last_requests'] = $this->authication_model->getUserListResult(['is_approved'=>0],5); 
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/index');
        $this->load->view('admin/includes/footer');
    }
    public function settings()
    {
        $data['title'] = "settings";
        $data['panel_settings'] = $this->settings_model->get_panel_settings();
        $data['settings'] = $this->settings_model->get_settings();
        $data['general_settings'] = $this->settings_model->get_general_settings();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/settings');
        $this->load->view('admin/includes/footer');
    }
    /**
     * Recaptcha Settings Post
     */
    public function recaptcha_settings_post()
    {
        if ($this->settings_model->update_recaptcha_settings()) {
            $this->session->set_flashdata('success', "Setting Updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable To Update Settings");
            redirect($this->agent->referrer());
        }
    }

    /**
     * Settings Post
     */
    public function settings_post()
    {
        if ($this->settings_model->update_settings()) {
            $this->settings_model->update_general_settings();
            $this->session->set_flashdata('success', "Setting Updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable To Update Settings");
            redirect($this->agent->referrer());
        }
    }

    public function emailSettings()
    {
        $data['title'] = "Email Settings";
        $data['general_settings'] = $this->settings_model->get_general_settings();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/email-settings');
        $this->load->view('admin/includes/footer');
    }

    /**
     * Email Settings Post
     */
    public function emailSettings_post()
    {
        if ($this->settings_model->update_email_settings()) {
            $this->session->set_flashdata('success', "Setting Updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable To Update Settings");
            redirect($this->agent->referrer());
        }
    }

    /*
    *PAGINATION SETTING
    */
    public function paginationSettings()
    {
        $data['title'] = "Pagination Settings";
        $data['general_settings'] = $this->settings_model->get_general_settings();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/pagination-settings');
        $this->load->view('admin/includes/footer');
    }

    /**
     * PAGINATION Settings Post
     */
    public function paginationSettings_post()
    {
        if ($this->settings_model->update_pagination_settings()) {
            $this->session->set_flashdata('success', "Setting Updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable To Update Settings ");
            redirect($this->agent->referrer());
        }
    }






    /*
    *-------------------------------------------------------------------------------------------------
    * Visual SETTINGS
    *-------------------------------------------------------------------------------------------------
    */

    /*
    * Visual Settings
    */
    public function visual_settings()
    {
        $data['title'] = "visual settings";
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/visual_settings', $data);
        $this->load->view('admin/includes/footer');
    }

    /**
     * Visual Settings Post
     */
    public function visual_settings_post()
    {
        if ($this->settings_model->update_visual_settings()) {
            $this->session->set_flashdata('success',"updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "fail");
            redirect($this->agent->referrer());
        }
    }


 
    /*
    *-------------------------------------------------------------------------------------------------
    * Contact Message
    *-------------------------------------------------------------------------------------------------
    */
    public function contact_messages()
    {
        $data['title'] = "Contact Messages";
        $data['messages'] = $this->contact_model->get_contact_messages();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/contact_messages', $data);
        $this->load->view('admin/includes/footer');
    }

    /**
     * Delete Contact Message Post
     */
    public function delete_contact_message_post()
    {
        $id = $this->input->post('id', true);

        if ($this->contact_model->delete_contact_message($id)) {
            $this->session->set_flashdata('success', "Message Deleted");
        } else {
            $this->session->set_flashdata('error', "Unable to delete meassage");
        }
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * Bank SETTINGS
    *-------------------------------------------------------------------------------------------------
    */

    /*
    * Visual Settings
    */
    public function bank_settings()
    {
        $data['title'] = "Bank settings";
        $data['bank_settings'] = $this->settings_model->get_bank_settings();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/bank_settings', $data);
        $this->load->view('admin/includes/footer');
    }

    /**
     * Visual Settings Post
     */
    public function bank_settings_post()
    {
        if ($this->settings_model->update_bank_settings()) {
            $this->session->set_flashdata('success', "updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "fail");
            redirect($this->agent->referrer());
        }
    }

    /*
    * PROFILE
    */
    public function change_profile()
    {
        $data['user_data'] = user();
        $data['title'] = "Profile";
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/profile', $data);
        $this->load->view('admin/includes/footer');
    }

    public function save_profile()
    {
        $this->load->library('upload');
        $oldIng = $this->input->post('oldIng');
        $oldpan = $this->input->post('oldpan');
        $oldsign = $this->input->post('oldsign');
        $this->form_validation->set_rules('father_name', "Father's/husband's Name", 'required|xss_clean|min_length[4]|max_length[250]');
        $this->form_validation->set_rules('sex', "Sex", 'required|xss_clean');
        $this->form_validation->set_rules('occupation', "Occupation", 'required|xss_clean|min_length[4]|max_length[255]');
        $this->form_validation->set_rules('address', "Address", 'required|xss_clean');
        $this->form_validation->set_rules('state', "State", 'required|xss_clean');
        $this->form_validation->set_rules('pin', "PIN Code", 'required|xss_clean|min_length[6]|max_length[6]|numeric');
        $this->form_validation->set_rules('phone', "Phone Number", 'required|xss_clean|min_length[10]|max_length[11]|numeric');
        $this->form_validation->set_rules('bank_name', "Bank Name", 'required|xss_clean');
        $this->form_validation->set_rules('acno', "Account Number", 'required|xss_clean|numeric');
        $this->form_validation->set_rules('bank_ifsc', "Bank IFSC Code", 'required|xss_clean');
        $this->form_validation->set_rules('nomine_name', "Nomine Name", 'required|xss_clean');
        $this->form_validation->set_rules('relation', "Relation", 'required|xss_clean');
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            $data = array(
                'father_name' => $this->input->post('father_name', true),
                'full_name' => $this->input->post('full_name', true),
                'sex' => $this->input->post('sex', true),
                'occupation' => $this->input->post('occupation', true),
                'address' => $this->input->post('address', true),
                'state' => $this->input->post('state', true),
                'pin' => $this->input->post('pin', true),
                'phone' => $this->input->post('phone', true),
                'bank_name' => $this->input->post('bank_name', true),
                'acno' => $this->input->post('acno', true),
                'bank_ifsc' => $this->input->post('bank_ifsc', true),
                'nomine_name' => $this->input->post('nomine_name', true),
                'age' => $this->input->post('age', true),
                'pan' => $this->input->post('pan', true),
                'mobile' => $this->input->post('mobile', true),
                'dob' => $this->input->post('dob', true),
            );
            if ($_FILES['avatar']['name'] != '') {
                $path = FCPATH . 'uploads/member-doc/';
                if (!is_dir($path)) {
                    mkdir($path, 0755, TRUE);
                }
                $config_prof['encrypt_name']  = TRUE;
                $config_prof['upload_path']   = $path;
                $config_prof['allowed_types'] = 'png|jpeg|jpg';
                $config['max_size']           = '5000';
                $config_prof['overwrite']     = true;
                $this->upload->initialize($config_prof);
                if ($this->upload->do_upload('avatar')) {
                    $file_data = $this->upload->data();
                    $this->resizeImage($file_data);
                    $data["avatar"] =  $file_data['file_name'];
                    if (!empty($oldIng)) {
                        delete_file_from_server('uploads/member-doc/' . $oldIng);
                    }
                } else {
                    $this->session->set_flashdata('error',  $this->upload->display_errors());
                    redirect($this->agent->referrer());
                }
            }
            if ($_FILES['pan_img']['name'] != '') {
                $path = FCPATH . 'uploads/member-doc/';
                if (!is_dir($path)) {
                    mkdir($path, 0755, TRUE);
                }
                $config_prof['encrypt_name']  = TRUE;
                $config_prof['upload_path']   = $path;
                $config_prof['allowed_types'] = 'png|jpeg|jpg';
                $config['max_size']           = '5000';
                $config_prof['overwrite']     = true;
                $this->upload->initialize($config_prof);
                if ($this->upload->do_upload('pan_img')) {
                    $file_data = $this->upload->data();
                    $this->resizeImage($file_data);
                    $data["pan_img"] =  $file_data['file_name'];
                    if (!empty($oldpan)) {
                        delete_file_from_server('uploads/member-doc/' . $oldpan);
                    }
                } else {
                    $this->session->set_flashdata('error',  $this->upload->display_errors());
                    redirect($this->agent->referrer());
                }
            }
            if ($_FILES['sign_img']['name'] != '') {
                $path = FCPATH . 'uploads/member-doc/';
                if (!is_dir($path)) {
                    mkdir($path, 0755, TRUE);
                }
                $config_prof['encrypt_name']  = TRUE;
                $config_prof['upload_path']   = $path;
                $config_prof['allowed_types'] = 'png|jpeg|jpg';
                $config['max_size']           = '5000';
                $config_prof['overwrite']     = true;
                $this->upload->initialize($config_prof);
                if ($this->upload->do_upload('sign_img')) {
                    $file_data = $this->upload->data();
                    $this->resizeImage($file_data);
                    $data["sign_img"] =  $file_data['file_name'];
                    if (!empty($oldsign)) {
                        delete_file_from_server('uploads/member-doc/' . $oldsign);
                    }
                } else {
                    $this->session->set_flashdata('error',  $this->upload->display_errors());
                    redirect($this->agent->referrer());
                }
            }
            if ($this->authication_model->updateUsers($data, $this->session->userdata('bp_sess_user_id'))) {
                $this->session->set_flashdata('success', "Profile Updated");
                redirect($this->agent->referrer());
            } else {

                $this->session->set_flashdata('error', "Unable update profile!");
                redirect($this->agent->referrer());
            }
        }
    }

    public function resizeImage($file_data)
    {
        $this->load->library('image_lib');
        $path = FCPATH . "/uploads/member-doc/";
        $height = 480;
        $width = 640;
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_data["raw_name"] . $file_data['file_ext'];
        $config['new_image'] = $path . $file_data["raw_name"] . $file_data['file_ext'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = "60";
        $config['width']         = $width;
        $config['height']       = $height;
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

      /*
    *-------------------------------------------------------------------------------------------------
    * Business SETTINGS
    *-------------------------------------------------------------------------------------------------
    */
    public function business_settings()
    {
        $data['title'] = "Business Plan settings";
        $data['business_settings'] = $this->businessplan_model->get_all_business_plans();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/business/business_settings', $data);
        $this->load->view('admin/includes/footer');
    }

    public function add_business_plan()
    {
        $data['title'] = "Add Business Plan";
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/business/add_new');
        $this->load->view('admin/includes/footer');
    }

    /**
     * Add blog Category post
     */
    public function add_business_plan_post()
    {
        //validate inputs
        $this->form_validation->set_rules('level_step', "Step Leavel", 'required|xss_clean|is_unique[business_settings.level_step]');
        $this->form_validation->set_rules('designation', "Designation", 'required|xss_clean');
        $this->form_validation->set_rules('left_leg', "Left Leg", 'required|xss_clean|numeric');
        $this->form_validation->set_rules('right_leg', "Right Leg", 'required|xss_clean|numeric');
        $this->form_validation->set_rules('anount', "Amount", 'required|xss_clean');
        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            if ($this->businessplan_model->add_business_plan()) {
                $this->session->set_flashdata('success', "Plan Added Successfuly");
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('error', "Unable To Add Plan");
                redirect($this->agent->referrer());
            }
        }
    }



    /**
     * edit blog  team
     */
    public function edit_business_plan($id)
    {
        
        $data['title'] = "Update Business Plan";
        $data['member'] = $this->businessplan_model->get_business_plan_by_id($id);
        if (empty($data['member'])) {
            redirect($this->agent->referrer());
        }
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/business/edit_plan', $data);
        $this->load->view('admin/includes/footer');
    }

    /**
     * edit blog team post
     */
    public function edit_business_plan_post()
    {
        //validate inputs
        $this->form_validation->set_rules('level_step', "Step Leavel", 'required|xss_clean');
        $this->form_validation->set_rules('designation', "Designation", 'required|xss_clean');
        $this->form_validation->set_rules('left_leg', "Left Leg", 'required|xss_clean|numeric');
        $this->form_validation->set_rules('right_leg', "Right Leg", 'required|xss_clean|numeric');
        $this->form_validation->set_rules('anount', "Amount", 'required|xss_clean');
        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
           
            $id = $this->input->post('id', true);
            if ($this->businessplan_model->update($id)) {
                $this->session->set_flashdata('success', "Plan Updated Successfuly");
                redirect(admin_url() . 'business-settings');
            } else {
                $this->session->set_flashdata('error', "Unable To Update ");
                redirect($this->agent->referrer());
            }
        }
    }
    public function delete_businessplan()
    {
        $id = $this->input->post('id', true);
        //check blog posts
        if ($this->businessplan_model->delete_business_plan($id)) {
            $this->session->set_flashdata('success', "Plan Deleted");
        } else {
            $this->session->set_flashdata('error', "Unable To delete Plan");
        }
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * Comapny Reward SETTINGS
    *-------------------------------------------------------------------------------------------------
    */
    public function reward_settings()
    {
        $data['title'] = "Reward Plan settings";
        $data['business_settings'] = $this->reward_model->get_all_reward_plans();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/reward_settings/index', $data);
        $this->load->view('admin/includes/footer');
    }

    public function add_reward_plan()
    {
        $data['title'] = "Add Reward Plan";
        $data['business_settings'] = $this->businessplan_model->get_all_business_plans();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/reward_settings/add_new');
        $this->load->view('admin/includes/footer');
    }

    /**
     * Add blog Category post
     */
    public function add_reward_plan_post()
    {
        //validate inputs
        $this->form_validation->set_rules('level_step', "Step Leavel", 'required|xss_clean');
        $this->form_validation->set_rules('timeline', "Time", 'required|xss_clean|numeric');
        $this->form_validation->set_rules('reward_name', "Reward name", 'required|xss_clean');
        $this->form_validation->set_rules('price', "Amount", 'required|xss_clean');
        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            if ($this->reward_model->add_reward_plan()) {
                $this->session->set_flashdata('success', "Plan Added Successfuly");
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('error', "Unable To Add Plan");
                redirect($this->agent->referrer());
            }
        }
    }

   

    /**
     * edit blog  team
     */
    public function edit_reward_plan($id)
    {

        $data['title'] = "Update Reward Plan";
        $data['business_settings'] = $this->businessplan_model->get_all_business_plans();
        $data['member'] = $this->reward_model->get_reward_plan_by_id($id);
        if (empty($data['member'])) {
            redirect($this->agent->referrer());
        }
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/reward_settings/edit_plan', $data);
        $this->load->view('admin/includes/footer');
    }

    /**
     * edit blog team post
     */
    public function edit_reward_plan_post()
    {
        //validate inputs
        $this->form_validation->set_rules('level_step', "Step Leavel", 'required|xss_clean');
        $this->form_validation->set_rules('timeline', "Time", 'required|xss_clean|numeric');
        $this->form_validation->set_rules('reward_name', "Reward name", 'required|xss_clean');
        $this->form_validation->set_rules('price', "Amount", 'required|xss_clean');
        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {

            $id = $this->input->post('id', true);
            if ($this->reward_model->update($id)) {
                $this->session->set_flashdata('success', "Plan Updated Successfuly");
                redirect(admin_url() . 'comapny-reward-settings');
            } else {
                $this->session->set_flashdata('error', "Unable To Update ");
                redirect($this->agent->referrer());
            }
        }
    }
    public function delete_reward_plan()
    {
        $id = $this->input->post('id', true);
        //check blog posts
        if ($this->reward_model->delete_reward_plan($id)) {
            $this->session->set_flashdata('success', "Plan Deleted");
        } else {
            $this->session->set_flashdata('error', "Unable To delete Plan");
        }
    }
    /*
    *-------------------------------------------------------------------------------------------------
    * Cashback Reward SETTINGS
    *-------------------------------------------------------------------------------------------------
    */

    public function cashback_settings()
    {

        $data['title'] = "Update Cashback Plan";
        $data['cashback_settings'] = $this->cashback_model->get_cashback_settings();
        
        if (empty($data['cashback_settings'])) {
            redirect($this->agent->referrer());
        }
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/reward_settings/edit_cashback', $data);
        $this->load->view('admin/includes/footer');
    }

    /**
     * edit blog team post
     */
    public function cashback_settings_post()
    {
        //validate inputs
        
        $this->form_validation->set_rules('total_member', "Total Member", 'required|xss_clean|numeric');
        $this->form_validation->set_rules('amount', "Amount", 'required|xss_clean');
        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            if ($this->cashback_model->update()) {
                $this->session->set_flashdata('success', "Plan Updated Successfuly");
                redirect(admin_url() . 'cashback-settings');
            } else {
                $this->session->set_flashdata('error', "Unable To Update ");
                redirect($this->agent->referrer());
            }
        }
    }

     /*
    *-------------------------------------------------------------------------------------------------
    * Transactions
    *-------------------------------------------------------------------------------------------------
    */

    public function transactions()
    {
        $data['title'] = "Total Transactions";
        $data['transactions'] = $this->transaction_model->get_all_trannsactions();

        if (empty($data['transactions'])) {
            redirect($this->agent->referrer());
        }
        $data['pl_status'] = $this->transaction_model->getTnsPL();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/transactions', $data);
        $this->load->view('admin/includes/footer');
    }

    public function filter_transactions()
    {
        $formInp = $this->input->post('form');
        $toInp = $this->input->post('to');
        $form = $formInp.' 00:00:00';
        $to  = date("Y-m-d H:i:s",strtotime($toInp));
        $where = " created_on >= '" . $form . "' AND created_on <= '" . $to . "' ORDER BY id DESC";
        $data['title'] = "Total Transactions";
        $sql = "SELECT * FROM transaction WHERE";
        $sql .= $where;
        $data['transactions'] = $this->db->query($sql)->result();
        $data['pl_status'] = $this->transaction_model->getTnsPL($where);
        $data['form_date'] = $formInp;
        $data['to_date'] = $toInp;
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/filter_transactions', $data);
        $this->load->view('admin/includes/footer');
    }
    /*
    *-------------------------------------------------------------------------------------------------
    * Payouts
    *-------------------------------------------------------------------------------------------------
    */
    public function payouts()
    {
        $data['title'] = "Payouts";
        $data['transactions'] = $this->payout_model->get_all_payouts();
        $data['pay_pending'] = $this->payout_model->get_all_payouts_count(array('status'=>0));
        $data['pay_done'] = $this->payout_model->get_all_payouts_count(array('status'=>1));
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/payout', $data);
        $this->load->view('admin/includes/footer');
    }

    public function filter_payout()
    {
        $formInp = $this->input->post('form');
        $toInp = $this->input->post('to');
        $form = $formInp . ' 00:00:00';
        $to  = $toInp . ' 23:59:59';
        $where = " created_at >= '" . $form . "' AND created_at <= '" . $to . "' ORDER BY id DESC";
        $data['title'] = "Filter Payouts";
        $sql = "SELECT * FROM payout_tracking WHERE";
        $sql .= $where;
        $data['transactions'] = $this->db->query($sql)->result();
        $data['form_date'] = $formInp;
        $data['to_date'] = $toInp;
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/filter_payout', $data);
        $this->load->view('admin/includes/footer');
    }

    public function pending_payouts()
    {
        $data['title'] = "Pending Payouts";
        $data['transactions'] = $this->payout_model->get_all_payouts(array('status' => 0));
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/pending_payout', $data);
        $this->load->view('admin/includes/footer');
    }

    public function release_payout($id)
    {
        $id = clean_number($id);
        $payoutData = $this->payout_model->get_payout_by_id($id);
        $tranactionArr = [
            'form_user_id' => 1,
            'to_user_id' => $payoutData->user_id,
            'created_on' => date("Y-m-d H:i:s"),
            'tranaction_date' => date("Y-m-d H:i:s"),
            'dr' => 0,
            'cr' => $payoutData->amount,
            'perticullars' => $payoutData->description,
        ];
        $updateArr =[
            'payment_on' => date("Y-m-d H:i:s"),
            'status'=>1
        ];
        if($this->transaction_model->add_transaction($tranactionArr)) {
            $this->payout_model->payout_update($updateArr,$id);
            $this->session->set_flashdata('success', "Payment Released");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable Release Payment");
            redirect($this->agent->referrer());
        }
    }

    public function release_bulk_payout()
    {

        $user_ids = $this->input->post('user_ids');
        if(!empty($user_ids)){
            foreach ($user_ids as $id) {
                $this->do_payout_release($id);
                $this->session->set_flashdata('success', "All Payment Released");
                redirect($this->agent->referrer());
            }
        }else{
            $this->session->set_flashdata('error', "No Payment Selected");
            redirect($this->agent->referrer());
        }
    }
    
    protected function do_payout_release($id)
    {
        $id = clean_number($id);
        $payoutData = $this->payout_model->get_payout_by_id($id);
        $tranactionArr = [
            'form_user_id' => 1,
            'to_user_id' => $payoutData->user_id,
            'created_on' => date("Y-m-d H:i:s"),
            'tranaction_date' => date("Y-m-d H:i:s"),
            'dr' => 0,
            'cr' => $payoutData->amount,
            'perticullars' => $payoutData->description,
        ];
        $updateArr = [
            'payment_on' => date("Y-m-d H:i:s"),
            'status' => 1
        ];
        $this->transaction_model->add_transaction($tranactionArr);
        $this->payout_model->payout_update($updateArr,$id);
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * Reward
    *-------------------------------------------------------------------------------------------------
    */
    public function rewards()
    {
        $data['title'] = "Reward Report";
        $data['transactions'] = $this->payout_model->get_all_rewards();
        $data['pay_pending'] = $this->payout_model->get_all_rewards_count(array('status' => 0));
        $data['pay_done'] = $this->payout_model->get_all_rewards_count(array('status' => 1));
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/reward', $data);
        $this->load->view('admin/includes/footer');
    }

    public function filter_reward()
    {
        $formInp = $this->input->post('form');
        $toInp = $this->input->post('to');
        $form = $formInp . ' 00:00:00';
        $to  = $toInp . ' 23:59:59';
        $where = " created_at >= '" . $form . "' AND created_at <= '" . $to . "' ORDER BY id DESC";
        $data['title'] = "Filter Reward";
        $sql = "SELECT * FROM company_rewad_tracking WHERE";
        $sql .= $where;
        $data['transactions'] = $this->db->query($sql)->result();
        $data['form_date'] = $formInp;
        $data['to_date'] = $toInp;
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/filter_reward', $data);
        $this->load->view('admin/includes/footer');
    }

    public function pending_reward()
    {
        $data['title'] = "Pending Reward";
        $data['transactions'] = $this->payout_model->get_all_rewards(array('status' => 0));
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/pending_reward', $data);
        $this->load->view('admin/includes/footer');
    }

    public function release_reward($id)
    {
        $id = clean_number($id);
        $payoutData = $this->payout_model->get_reward_by_id($id);
        $tranactionArr = [
            'form_user_id' => 1,
            'to_user_id' => $payoutData->user_id,
            'created_on' => date("Y-m-d H:i:s"),
            'tranaction_date' => date("Y-m-d H:i:s"),
            'dr' => 0,
            'cr' => $payoutData->amount,
            'perticullars' => $payoutData->description,
        ];
        $updateArr = [
            'payment_on' => date("Y-m-d H:i:s"),
            'status' => 1
        ];
        
        if ($this->transaction_model->add_transaction($tranactionArr)) {
            $this->payout_model->reward_update($updateArr, $id);
            $this->session->set_flashdata('success', "Payment Released");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable Release Payment");
            redirect($this->agent->referrer());
        }
    }

    public function release_bulk_reward()
    {

        $user_ids = $this->input->post('user_ids');
        if (!empty($user_ids)) {
            foreach ($user_ids as $id) {
                $this->do_reword_release($id);
                $this->session->set_flashdata('success', "All Payment Released");
                redirect($this->agent->referrer());
            }
        } else {
            $this->session->set_flashdata('error', "No Payment Selected");
            redirect($this->agent->referrer());
        }
    }

    protected function do_reword_release($id)
    {
        $id = clean_number($id);
        $payoutData = $this->payout_model->get_reward_by_id($id);
        $tranactionArr = [
            'form_user_id' => 1,
            'to_user_id' => $payoutData->user_id,
            'created_on' => date("Y-m-d H:i:s"),
            'tranaction_date' => date("Y-m-d H:i:s"),
            'dr' => 0,
            'cr' => $payoutData->amount,
            'perticullars' => $payoutData->description,
        ];
        $updateArr = [
            'payment_on' => date("Y-m-d H:i:s"),
            'status' => 1
        ];
        $this->transaction_model->add_transaction($tranactionArr);
        $this->payout_model->reward_update($updateArr, $id);
    }
    /*
    *-------------------------------------------------------------------------------------------------
    * Cashback
    *-------------------------------------------------------------------------------------------------
    */
    public function cashbacks()
    {
        $data['title'] = "Cash-Back Report";
        $data['transactions'] = $this->payout_model->get_all_cashbacks();
        $data['pay_pending'] = $this->payout_model->get_all_cashbacks_count(array('status' => 0));
        $data['pay_done'] = $this->payout_model->get_all_cashbacks_count(array('status' => 1));
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/cashback', $data);
        $this->load->view('admin/includes/footer');
    }

    public function filter_cashback()
    {
        $formInp = $this->input->post('form');
        $toInp = $this->input->post('to');
        $form = $formInp . ' 00:00:00';
        $to  = $toInp . ' 23:59:59';
        $where = " created_at >= '" . $form . "' AND created_at <= '" . $to . "' ORDER BY id DESC";
        $data['title'] = "Filter Reward";
        $sql = "SELECT * FROM cashback_user_trans WHERE";
        $sql .= $where;
        $data['transactions'] = $this->db->query($sql)->result();
        $data['form_date'] = $formInp;
        $data['to_date'] = $toInp;
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/filter_cashback', $data);
        $this->load->view('admin/includes/footer');
    }

    public function pending_cashback()
    {
        $data['title'] = "Pending Cash-Back";
        $data['transactions'] = $this->payout_model->get_all_cashbacks(array('status' => 0));
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/pending_cashback', $data);
        $this->load->view('admin/includes/footer');
    }

    public function release_cashback($id)
    {
        $id = clean_number($id);
        $payoutData = $this->payout_model->get_cashback_by_id($id);
        $tranactionArr = [
            'form_user_id' => 1,
            'to_user_id' => $payoutData->user_id,
            'created_on' => date("Y-m-d H:i:s"),
            'tranaction_date' => date("Y-m-d H:i:s"),
            'dr' => 0,
            'cr' => $payoutData->amount,
            'perticullars' => $payoutData->description,
        ];
        $updateArr = [
            'payment_on' => date("Y-m-d H:i:s"),
            'status' => 1
        ];

        if ($this->transaction_model->add_transaction($tranactionArr)) {
            $this->payout_model->cashback_update($updateArr, $id);
            $this->session->set_flashdata('success', "Payment Released");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable Release Payment");
            redirect($this->agent->referrer());
        }
    }

    public function release_bulk_cashback()
    {

        $user_ids = $this->input->post('user_ids');
        if (!empty($user_ids)) {
            foreach ($user_ids as $id) {
                $this->do_cashback_release($id);
                $this->session->set_flashdata('success', "All Payment Released");
                redirect($this->agent->referrer());
            }
        } else {
            $this->session->set_flashdata('error', "No Payment Selected");
            redirect($this->agent->referrer());
        }
    }

    protected function do_cashback_release($id)
    {
        $id = clean_number($id);
        $payoutData = $this->payout_model->get_cashback_by_id($id);
        $tranactionArr = [
            'form_user_id' => 1,
            'to_user_id' => $payoutData->user_id,
            'created_on' => date("Y-m-d H:i:s"),
            'tranaction_date' => date("Y-m-d H:i:s"),
            'dr' => 0,
            'cr' => $payoutData->amount,
            'perticullars' => $payoutData->description,
        ];
        $updateArr = [
            'payment_on' => date("Y-m-d H:i:s"),
            'status' => 1
        ];
        $this->transaction_model->add_transaction($tranactionArr);
        $this->payout_model->cashback_update($updateArr, $id);
    }

        /*
    *-------------------------------------------------------------------------------------------------
    * MLM SECTION REPORT
    *-------------------------------------------------------------------------------------------------
    */
    public function mlm_user()
    {
        $data['title'] = "MLM Users";
        $data['users'] = $this->authication_model->getUserListResult(['is_approved' => 1]);
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/mlm_users', $data);
        $this->load->view('admin/includes/footer');
        
    }

    public function edit_user_status($id)
    {
        $id = clean_number($id);
        if ($this->authication_model->ban_remove_ban_user($id)) {
            $this->session->set_flashdata('success', "User Updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable To Update ");
            redirect($this->agent->referrer());
        }
    }
    public function generology($id)
    {
        $id = clean_number($id);
        $returndata="[";
        /* $userLists = $this->authication_model->getUserListResult(['parrent_id' => $id]);
        foreach ($userLists as $user) {

        } */
       /*  echo "<pre>";
        $x = $this->getSubchild($id);
        print_r($x);exit;
        $data['title'] = "MLM TREE"; */
        $data['user_data'] = "MLM TREE";
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/mlm_tree', $data);
        $this->load->view('admin/includes/footer');
    }

/*     protected function makeLegChart($user_id){
        $row1 = [];
        $userLists = $this->authication_model->getUserListResult(['parrent_id' => $id]);
        if(!empty($config_profuserLists)){
        foreach ($userLists as $user) {
            
        }
        }
    }

    protected function getSubchild($user_id){
        $row1 = [];
        $deepak = $this->db->query("SELECT id,username,parrent_id,full_name FROM `users` WHERE parrent_id='" . $user_id . "'")->result_array();

        if ($deepak) {
            $count2 = 0;
            foreach ($deepak as $key => $value) {
                $id = $value['parrent_id'];
                $row1[$key]['mem_id'] = $value['id'];
                //$row1[$key]['placement'] = $value['reffer_from'];
                $row1[$key]['name'] = $value['full_name'];
                $row1[$key]['nodes'] = array_values($this->getSubchild($value['user_id']));
                print_r($row1);
            }
        }
        return $row1;
    }     */


}