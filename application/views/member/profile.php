<div class="wrapper">
    <div class="">
        <?php echo form_open_multipart('save-profile'); ?>
        <div class="wrapper-box">
            <h1 class="text-center">Profile</h1>
            <div class="col-sm-12">
                <?php $this->load->view('admin/includes/_messages'); ?>
            </div>
            <div class="wrapper-box-content">

                <div class="m-wrapper-content">

                    <div class="captcha">
                        <div class="captcha-content">
                            <div class="form-group">
                                <label>User ID</label>
                                <input type="text" name="username" value="<?php echo $user_data->username; ?>" class="form-control" placeholder="Heading1" disabled>
                            </div>
                            <div class="form-group">
                                <label>SPO.Id.no</label>
                                <input type="text" name="username" value="<?php echo get_user($user_data->spon_id)->username; ?>" class="form-control" placeholder="Heading1" disabled>
                            </div>
                            <div class="form-group">
                                <label>Name Of Applicant</label>
                                <input type="text" name="full_name" value="<?php echo $user_data->full_name; ?>" class="form-control" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label>Father's/husband's Name</label>
                                <input type="text" name="father_name" value="<?php echo $user_data->father_name; ?>" class="form-control" placeholder="Father's/husband's name">
                            </div>
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input type="text" name="dob" value="<?php echo date('d-m-Y', strtotime($user_data->dob)); ?>" class="form-control" placeholder="Date Of Birth" disabled>
                            </div>
                            <div class="form-group">
                                <label>Sex</label>
                                <strong>MALE</strong> <input type="radio" name="sex" value="male" required <?php echo ($user_data->sex == 'male') ? 'checked' : '' ?>>
                                <span> <strong> FEMALE </strong><input type="radio" name="sex" value="female" <?php echo ($user_data->sex == 'female') ? 'checked' : '' ?>></span>
                            </div>
                            <div class="form-group">
                                <label>Age</label>
                                <input type="text" name="age" value="<?php echo $user_data->age; ?>" class="form-control" disabled>
                            </div>

                            <div class="form-group">
                                <label>Occupation</label>
                                <input type="text" name="occupation" value="<?php echo $user_data->occupation; ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Address</label>
                                <textarea type="text" name="address" class="form-control"><?php echo $user_data->address; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label>State</label>
                                <input type="text" name="state" value="<?php echo $user_data->state; ?>" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>PIN Code</label>
                                <input type="text" name="pin" value="<?php echo $user_data->pin; ?>" class="form-control" maxlength="6">
                            </div>
                            <div class="form-group">
                                <label>Pan No</label>
                                <input type="text" name="pan" value="<?php echo $user_data->pan; ?>" class="form-control" maxlength="10" disabled>
                            </div>
                            <div class="form-group">
                                <label>Phone No
                                </label>
                                <input type="text" name="phone" value="<?php echo $user_data->phone; ?>" class="form-control" maxlength="11">
                            </div>
                            <div class="form-group">
                                <label>Mobile No</label>
                                <input type="text" name="mobile" value="<?php echo $user_data->mobile; ?>" class="form-control" maxlength="10" disabled>
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <input type="text" name="email" value="<?php echo $user_data->email; ?>" class="form-control" disabled>
                            </div>

                            <div class="form-group">
                                <label>Bank Name
                                </label>
                                <input type="text" name="bank_name" value="<?php echo $user_data->bank_name; ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Bank A/c No
                                </label>
                                <input type="text" name="acno" value="<?php echo $user_data->acno; ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Bank IFSC Code
                                </label>
                                <input type="text" name="bank_ifsc" value="<?php echo $user_data->bank_ifsc; ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Nominee Name</label>
                                <input type="text" name="nomine_name" value="<?php echo $user_data->nomine_name; ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Relation With Nominee</label>
                                <input type="text" name="relation" value="<?php echo $user_data->relation; ?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Photo</label>
                                <?php if (!empty($user_data->avatar)) : ?>

                                    <div class="row mx-auto">
                                        <div class="col-md-3">
                                            <img src="<?php echo base_url() . 'uploads/member-doc/' . $user_data->avatar ?>" style="width: 100%;padding: 10px 0;">
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="row  mx-auto">
                                        <div class="col-md-3">
                                            <img src="<?php echo base_url() . 'assets/backend/images/noimg.png' ?>" style="width: 100%;padding: 10px 0;">
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <input type="file" name="avatar" class="" value="" placeholder="Profile Image">

                            </div>
                            <div class="form-group">
                                <label>Signature</label>
                                <?php if (!empty($user_data->sign_img)) : ?>

                                    <div class="row mx-auto">
                                        <div class="col-md-3">
                                            <img src="<?php echo base_url() . 'uploads/member-doc/' . $user_data->sign_img ?>" style="width: 100%;padding: 10px 0;">
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="row  mx-auto">
                                        <div class="col-md-3">
                                            <img src="<?php echo base_url() . 'assets/backend/images/noimg.png' ?>" style="width: 100%;padding: 10px 0;">
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <input type="file" name="sign_img" class="" value="" placeholder="Profile Image">

                            </div>
                            <div class="form-group">
                                <label>PAN Card </label>
                                <?php if (!empty($user_data->pan_img)) : ?>

                                    <div class="row mx-auto">
                                        <div class="col-md-3">
                                            <img src="<?php echo base_url() . 'uploads/member-doc/' . $user_data->pan_img ?>" style="width: 100%;padding: 10px 0;">
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="row  mx-auto">
                                        <div class="col-md-3">
                                            <img src="<?php echo base_url() . 'assets/backend/images/noimg.png' ?>" style="width: 100%;padding: 10px 0;">
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <input type="file" name="pan_img" class="" value="" placeholder="Profile Image">

                            </div>
                            <input type="hidden" name="oldIng" value="<?php echo $user_data->avatar; ?>">
                            <input type="hidden" name="oldpan" value="<?php echo $user_data->pan_img; ?>">
                            <input type="hidden" name="oldsign" value="<?php echo $user_data->sign_img; ?>">
                            <div class="sav-btn">
                                <button>Update Changes</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>