<div class="col-sm-12">
    <?php $this->load->view('admin/includes/_messages'); ?>
</div>
<div class="wrapper2">
    <div class="mblog">
        <h4>Reject Application</h4>
        <?php echo form_open('application_controller/reject_member_post'); ?>
        <div class="form-group">
            <label>Rejection Cause</label>
            <textarea rows="10" name="rejection_cause" class="form-control" required></textarea>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $app_data->id ?>">
    <div class="sav-btn">
        <button type="submit">Save</button>
    </div>
    <?php echo form_close(); ?>
</div>