</div>
<div class="modal ajloder" id="ajaxloaderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <img src="<?php echo base_url() . 'assets/backend/images/project-loader-colors.gif' ?>">
            </div>

        </div>
    </div>
</div>
<footer>
    <p>© Copyright <?php echo date('Y') ?> <a href="<?php echo base_url() ?>" target="_blank"><?php echo $this->general_settings->application_name ?></a> | Designed & Developed By <a href="https://business.bigpage.in/" target="_blank">Bigpage</a> </p>
</footer>
</section>
<script src='http://code.jquery.com/jquery-latest.js'></script>
<script src="<?php echo base_url('assets/backend/js/bootstrap.min.js'); ?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/bp-scripts.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<!-- Jquery datatable tools js -->
<script type="text/javascript" src="<?php echo base_url('assets/backend/datatables/tools/jquery.dataTables.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/backend/datatables/tools/dataTables.buttons.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/backend/datatables/tools/jszip.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/backend/datatables/tools/pdfmake.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/backend/datatables/tools/vfs_fonts.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/backend/datatables/tools/buttons.html5.min.js'); ?>"></script>
<!-- dataTables Tools / -->
<script type="text/javascript" src="<?php echo base_url('assets/backend/datatables/dataTables.bootstrap.js'); ?>"></script>

<script>
    $(document).ready(function() {
        $('#example3, #example1, #example2').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5',
                'pdfHtml5'
            ],
            search: false
        });
    });
</script>




<script>
    $('#myModal').on('shown.bs.modal', 'keyboard: false', function() {
        $('#myInput').trigger('focus')
    })
</script>
<script>
    $(document).ready(function() {
        $('.counter-value').each(function() {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 3500,
                easing: 'swing',
                step: function(now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(function() {
        'use strict';
        (function() {
            var aside = $('.mside-nav'),
                showAsideBtn = $('.show-side-btn'),
                contents = $('#contents');
            showAsideBtn.on("click", function() {
                $("#" + $(this).data('show')).toggleClass('show-mside-nav');
                contents.toggleClass('margin');
            });
            if ($(window).width() <= 767) {
                aside.addClass('show-mside-nav');
            }
            $(window).on('resize', function() {
                if ($(window).width() > 767) {
                    aside.removeClass('show-mside-nav');
                }
            });
            // dropdown menu in the side nav
            var slideNavDropdown = $('.mside-nav-dropdown');
            $('.mside-nav .categories li').on('click', function() {
                $(this).toggleClass('opend').siblings().removeClass('opend');
                if ($(this).hasClass('opend')) {
                    $(this).find('.mside-nav-dropdown').slideToggle('fast');
                    $(this).siblings().find('.mside-nav-dropdown').slideUp('fast');
                } else {
                    $(this).find('.mside-nav-dropdown').slideUp('fast');
                }
            });
            $('.mside-nav .close-aside').on('click', function() {
                $('#' + $(this).data('close')).addClass('show-mside-nav');
                contents.removeClass('margin');
            });
        }());


    });
</script>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>
<script>
    $('#location_1').on('ifChecked', function() {
        $("#location_countries").hide();
    });
    $('#location_2').on('ifChecked', function() {
        $("#location_countries").show();
    });
    var sweetalert_ok = '<?php echo "ok"; ?>';
    var sweetalert_cancel = '<?php echo "cancel"; ?>';
</script>
</body>

</html>