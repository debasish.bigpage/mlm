<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Ajax_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function claculateAge()
    {
        $dob = $this->input->post('dob');
        $bday = new DateTime($dob);
        $age = $bday->diff(new DateTime);
        echo $age->y;
    }

}